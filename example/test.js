const options = {
    account: 'test',
    appid: 'wxa9287ae8ca583278',
    appsecret: 'b5cc39d030d739a94a24ed537256b2ad',
    token: 'wxz',
    redis_options: {
        host: '192.168.10.10'
    },
    cache: {
        drive: 'file',
        file: {
            path: `${process.env.PWD}/cache`
        },
        redis:{
            host: '192.168.10.10'
        },
    }
}
const path = require('path')
const Wechat = require('../index')
const wechat = new Wechat(options)

class Test {
    constructor() {
        this.init()
    }
    async init () {
        const file = path.join(__dirname,'jenny.jpg')
        console.log(file)
        const res = await wechat.mediaUpload(file, 'image')
        console.log(res)
    }
}

module.exports = new Test()