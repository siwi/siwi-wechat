module.exports = {
    REDIS_OPTIONS: {
        host: '192.168.0.103'
    },
    REDIS_PREFIX: 'SIWI_WECHAT:',
    SIWI_WECHAT_ERROR: 'SIWI_WECHAT:SIWI_WECHAT_ERROR',
    SIWI_WECHAT_ACCESS_TOKEN: 'SIWI_WECHAT_ACCESS_TOKEN',
    API_URL_PREFIX: 'https://api.weixin.qq.com/cgi-bin',
    AUTH_URL:'/token?grant_type=client_credential&',
    USER_INFO_URL: '/user/info'
}