const request = require('../modules/request')
class SubscribeMsg {
    constructor() {}

    /**
     * 一次性订阅消息
     * @param {*} template_id 
     * @param {*} sence 
     * @param {*} reserved 
     */
    async subscribeMsg(template_id, sence, reserved) {
        const uri = `https://mp.weixin.qq.com/mp/subscribemsg?action=get_confirm&appid=${this.appid}&scene=${scene}&template_id=${template_id}&redirect_url=${this.redirect_url}&reserved=${reserved}#wechat_redirect`
        return uri
    }

    /**
     * 推送订阅模板消息给到授权微信用户
     * @param {*} touser  	填接收消息的用户openid
     * @param {*} template_id 订阅消息模板ID
     * @param {*} scene 订阅场景值
     * @param {*} title 消息标题，15字以内
     * @param {*} template_data 消息正文，value为消息内容文本（200字以内），没有固定格式，可用\n换行，color为整段消息内容的字体颜色（目前仅支持整段消息为一种颜色）
     * @param {*} url 点击消息跳转的链接，需要有ICP备案
     * @param {*} miniprogram 跳小程序所需数据，不需跳小程序可不用传该数据
     */
    async templateSubscribe(touser, template_id, scene, title, template_data, url = false, miniprogram = false) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/message/template/subscribe?access_token=${access_token}`
        const data = {
            touser: touser,
            template_id: template_id,
            scene: scene,
            title: title,
            data: {
                content: {
                    value: template_data['value'],
                    color: template_data['color']
                }
            }
        }
        // 跳转页面
        if (url) {
            data['url'] = url
        }
        // 跳转小程序
        if (miniprogram) {
            data['miniprogram'] = {
                appid: miniprogram['appid'],
                pagepath: miniprogram['pagepath']
            }
        }
        const res = await request.post(uri, data)
        return res
    }
}

module.exports = SubscribeMsg