/**
 * @author [siwi]
 * @email [siwi@siwi.me]
 * @create date 2018-03-15 05:53:13
 * @modify date 2018-03-15 05:53:13
 * @desc [siwi-wechat resource]
 */
const request = require('../modules/request')
class Resource {
    constructor() {

    }
    /**
     * 上传素材
     * 微信接口：https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE
     * @param {*} file 
     * @param {*} type 
     */
    async mediaUpload(file, type) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token=${access_token}&type=${type}`
        const data = {
            file: file
        }

        const res = await request.file(uri, data)
        return res
    }

    /**
     * 获取临时素材
     * 微信接口: https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID
     * @param {*} media_id 
     */
    async mediaGet(media_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/media/get?access_token=${access_token}&media_id=${media_id}`

        // const res = await request.get(uri)
        return uri
    }

    /*
    {
        "articles":[
            {
                "title": TITLE,
                "thumb_media_id": THUMB_MEDIA_ID,
                "author": AUTHOR, 
                "digest" : DIGEST,
                "show_cover_pic": SHOW_COVER_PIC（0/1）,
                "content" : CONTENT,
                "content_source_url": CONTENT_SOURCE_URL,
                "need_open_comment" : NEED_OPEN_COMMENT（0/1）,
                "only_fans_can_comment" : ONLY_FANS_CAN_COMMENT（0/1
            },
            //若新增的是多图文素材，则此处应有几段articles结构，最多8段 
        ]
            
    }
    */

    /**
     * 新增永久图文素材
     * 
    */
    async materialAddNews(articles) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=${access_token}`
        const data = {
            articles: articles
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 上传图文消息内的图片获取URL
     * 微信接口：https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN
     */
    async mediaUploadImg(file) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=${access_token}`
        const data = {
            file: file
        }
        const res = await request.file(uri, data)
        return res
    }
    /**
     * 新增永久素材
     * @param {*} file 
     * @param {*} type 
     */
    async materialAddMaterial(file, type) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=${access_token}&type=${type}`
        
        const data = {
            file: file,
        }

        const res = await request.file(uri, data)
        return res
    }

    /**
     *  获取永久素材
     *  微信接口：https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=ACCESS_TOKEN
     * @param {String} media_id 
     */
    async materialGetMaterial(media_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=${access_token}&type=${type}`
        const data = {
            media_id: media_id
        }

        const res = await request.post(uri, data)
        return res
    }

    /**
     * 删除永久素材
     * 微信接口：https://api.weixin.qq.com/cgi-bin/material/del_material?access_token=ACCESS_TOKEN
     * @param {String} media_id 
     */
    async materialDelMaterial(media_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/material/del_material?access_token=${access_token}`
        const data = {
            media_id: media_id
        }
        const res = await request.json(uri, data)
        return res
    }

    // 更新图文素材 articles 
    /**
    {
        "media_id": "fS2dJtM25dKQbNmTGRbYoJTm_VKhWgDwmtFgNsRySUY",
        "index": 0,
        "articles": {
            "title": "图文测试修改",
            "thumb_media_id": "fS2dJtM25dKQbNmTGRbYoIEDVadVE3dzE6RkcP4LqrU",
            "author": "Mankong",
            "digest": "这是很好的测试修改 你好啊",
            "show_cover_pic": 1,
            "content": "你好啊, 这是很好的测试 多谢你了",
            "content_source_url": "https://siwi.me"
        }
    }
    */

   /**
    * 修改永久图文素材
    * @param {*} media_id 
    * @param {*} index 
    * @param {*} articles 
    */
    async materialUpdateNews(media_id, index, articles) {

        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/material/update_news?access_token=${access_token}`
        const data = {
            media_id: media_id,
            index: index,
            articles: articles
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取素材总数
     * 
     */
    async materialGetMaterialCount() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=${access_token}`
        
        const res = await request.get(uri)
        return res
    }

    /**
     * 获取素材列表
     * 微信接口：https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=ACCESS_TOKEN
     * @param {String} type  素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news）
     * @param {Number} offset 从全部素材的该偏移位置开始返回，0表示从第一个素材 返回
     * @param {Number} count 返回素材的数量，取值在1到20之间
     */
    async materialBatchGetMaterial(type, offset = 0, count = 20) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=${access_token}`
        const data = {
            type: type,
            offset:offset,
            count:count
        }
        const res = await request.json(uri, data)
        return res
    } 
}

module.exports = Resource