const request = require('../modules/request')

class DataCube {
    constructor() {}

    /**
     * 获取用户增减数据
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataCubeGetUserSummary(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getusersummary?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取累计用户数据
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataCubeGetUserCumulate(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getusercumulate?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取图文群发每日数据
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetArticleSummary(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getarticlesummary?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取图文群发总数据（getarticletotal）
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetArticleTotal(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getarticletotal?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }
    /**
     * 获取图文统计数据（getuserread）
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetUserRead(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getuserread?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取图文统计分时数据（getuserreadhour）
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetUserReadHour(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getuserreadhour?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }
    /**
     * 获取图文分享转发数据（getusershare）	
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetUserShare(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getusershare?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取图文分享转发数据（getusersharehour）	
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetUserShareHour(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getusersharehour?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }


    /**
     * 获取消息发送概况数据（getupstreammsg）
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetUpStreamMsg(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getupstreammsg?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取消息分送分时数据（getupstreammsghour)
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetUpStreamMsgHour(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getupstreammsghour?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取消息发送周数据（getupstreammsgweek）
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetUpStreamMsgWeek(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getupstreammsgweek?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     *获取消息发送月数据（getupstreammsgmonth）
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetUpStreamMsgMonth(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getupstreammsgmonth?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取消息发送分布数据（getupstreammsgdist）
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetUpStreamMsgDist(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getupstreammsgdist?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取消息发送分布周数据（getupstreammsgdistweek）
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetUpStreamMsgDistWeek(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getupstreammsgdistweek?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }
    /**
     * 获取消息发送分布月数据（getupstreammsgdistmonth）
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetUpStreamMsgDistMonth(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getupstreammsgdistmonth?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取接口分析数据（getinterfacesummary）
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetInterfaceSummary(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getinterfacesummary?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }
    /**
     * 获取接口分析分时数据（getinterfacesummaryhour）
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async dataGetInterfaceSummaryHour(begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getinterfacesummaryhour?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /* 卡券统计 */
    /**
     * 拉取卡券概况数据接口
     * @param {*} begin_date 
     * @param {*} end_date 
     * @param {*} cond_source 
     */
    async dataCubeGetCardBizuinInfo(begin_date, end_date, cond_source) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getcardbizuininfo??access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date,
            cond_source: cond_source
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取免费券数据接口
     * @param {*} begin_date 
     * @param {*} end_date 
     * @param {*} cond_source 
     * @param {*} card_id 
     */
    async dataCubeGetCardCardInfo(begin_date, end_date, cond_source, card_id = false) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getcardcardinfo?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date,
            cond_source: cond_source
        }
        if (card_id) {
            data['card_id'] = card_id
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 拉取会员卡概况数据接口
     * @param {*} begin_date 
     * @param {*} end_date 
     * @param {*} cond_source 
     */
    async dataCubeGetCardMemberCardInfo (begin_date, end_date, cond_source) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getcardmembercardinfo?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date,
            cond_source: cond_source
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 拉取单张会员卡数据接口
     * @param {*} begin_date 
     * @param {*} end_date 
     * @param {*} card_id 
     */
    async dataCubeGetCardMemberCardDetail (begin_date, end_date, card_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/datacube/getcardmembercarddetail?access_token=${access_token}`
        const data = {
            begin_date: begin_date,
            end_date: end_date,
            card_id: card_id
        }
        const res = await request.json(uri, data)
        return res
    }
    /**
     * 检测时间跨度 TODO
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async checkTimeSpan(begin_date, end_date) {
        return true
    }

}

module.exports = DataCube