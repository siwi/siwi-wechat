const request = require('../modules/request')
class Customservice {
    constructor() {}
    /**
     * 添加客服帐号
     * @param {*} kf_account 
     * @param {*} nickname 
     * @param {*} password 
     */
    async kfAccountAdd(kf_account, nickname, password) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/customservice/kfaccount/add?access_token=${access_token}`
        const data = {
            kf_account: kf_account,
            nickname: nickname,
            password: password,
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 修改客服账号
     * @param {*} kf_account 
     * @param {*} nickname 
     * @param {*} password 
     */
    async kfAccountUpdate(kf_account, nickname, password) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/customservice/kfaccount/update?access_token=${access_token}`
        const data = {
            kf_account: kf_account,
            nickname: nickname,
            password: password,
        }

        const res = await request.json(uri, data)
        return res
    }
    /**
     * 删除客服账号
     * @param {*} kf_account 
     * @param {*} nickname 
     * @param {*} password 
     */
    async kfAccountDel(kf_account, nickname, password) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/customservice/kfaccount/del?access_token=${access_token}`
        const data = {
            kf_account: kf_account,
            nickname: nickname,
            password: password,
        }

        const res = await request.post(uri, data)
        return res
    }

    /**
     * 设置客服帐号的头像 TODO
     * @param {*} file 
     * @param {*} kf_account 
     */
    async kfAccountUploadHeadImg(file, kf_account) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        // http://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token=ACCESS_TOKEN&kf_account=KFACCOUNT
        const uri = `https://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token=${access_token}&kf_account=${kf_account}`
        const data = {
            access_token: access_token,
        }
        const res = await request.post(uri, data)
        return res
    }
    /**
     * 获取客服列表
     */
    async getKfList() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token=${access_token}`
        
        const res = await request.get(uri)
        return res
    }

    /**
     * 发送客服消息
     * https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN
     * @param {*} 
     */
    async customSend(data) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=${access_token}`
        
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 客服输入状态
     * @param {openid} touser 
     * @param {"Typing"：对用户下发“正在输入"状态 "CancelTyping"：取消对用户的”正在输入"状态} command 
     */
    async typing(touser, command) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/message/custom/typing?access_token=${access_token}`
        const data = {
            touser: touser,
            command: command
        }

        const res = await request.json(uri, data)
        return res
    }
}

module.exports = Customservice