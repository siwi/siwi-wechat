const request = require('../modules/request')
class Poi {
    constructor() {

    }

    /**
     * 创建门店
     * @param {*} json 
     */
    async poiAddPoi(data) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `http://api.weixin.qq.com/cgi-bin/poi/addpoi?access_token=${access_token}`
        
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 查询门店信息
     * @param {*} json 
     */
    async poiGetPoi(poi_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `http://api.weixin.qq.com/cgi-bin/poi/getpoi?access_token=${access_token}`
        const data = {
            access_token: access_token,
            poi_id: poi_id
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 
     * @param {*} begin 
     * @param {*} limit 
     */
    async poiGetPoiList(begin, limit) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/poi/getpoilist?access_token=${access_token}`
        const data = {
            begin: begin,
            limit: limit
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 修改门店服务信息
     * @param {*} data 
     */
    async poiUpdatePoi(data) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/poi/updatepoi?access_token=${access_token}`
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 删除门店服务信息
     * @param {*} json 
     */
    async poiDelPoi(poi_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/poi/delpoi?access_token=${access_token}`
        const data = {
            poi_id: poi_id
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 门店类目表
     */
    async poiGetWxCategory() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `http://api.weixin.qq.com/cgi-bin/poi/getwxcategory?access_token=${access_token}`
        const res = await request.get(uri)
        return res
    }

    /**
     * 拉取门店小程序类目
     */
    async wxaGetMerchantCategory() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/get_merchant_category?access_token=${access_token}`
        const res = await request.get(uri)
        return res
    }

    /**
     * 创建门店小程序
     * @param {*} data 
     */
    async wxaApplyMerchant(data) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/apply_merchant?access_token=${access_token}`
        
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 查询门店小程序审核结果
     */
    async wxaGetMerchantAuditInfo() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/get_merchant_audit_info?access_token=${access_token}`
        const res = await request.get(uri)
        return res
    }

    /**
     * 修改门店小程序信息
     * @param {*} headimg_mediaid 
     * @param {*} intro 
     */
    async wxaModifyMerchant(headimg_mediaid, intro) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/modify_merchant?access_token=${access_token}`
        const data = {
            headimg_mediaid: headimg_mediaid,
            intro: intro
        }
        const res = await request.post(uri, data)
        return res
    }
    /**
     * 从腾讯地图拉取省市区信息
     */
    async wxaGetDistrict(data) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/get_district?access_token=${access_token}`
        const res = await request.get(uri)
        return res
    }


    /**
     * 在腾讯地图中搜索门店
     * @param {*} districtid 
     * @param {*} keyword 
     */
    async wxaSearchMapPoi(districtid, keyword) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/search_map_poi?access_token=${access_token}`
        const data = {
            districtid: districtid,
            keyword: keyword,
        }
        const res = await request.json(uri, data)
        return res
    }

   /**
    * 在腾讯地图中创建门店
    * @param {*} data 
    */
    async wxaCreateMapPoi(data) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/create_map_poi?access_token=${access_token}`
       
        const res = await request.post(uri, data)
        return res
    }

    /**
     * 添加门店
     * @param {*} data 
     */
    async wxaAddStore(data) {

        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/add_store?access_token=${access_token}`
        
        const res = await request.post(uri, data)
        return res
    }



    /**
     * 更新门店信息
     * 
     */
    async wxaUpdateStore(data) {

        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/update_store?access_token=${access_token}`
        const res = await request.post(uri, data)
        return res
    }

    /**
     * 获取单个门店信息
     * @param {*} poi_id 
     */
    async wxaGetStoreInfo(poi_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/get_store_info?access_token=${access_token}`
        const data = {
            poi_id: poi_id
        }
        const res = await request.post(uri, data)
        return res
    }

    /**
     * 获取单个门店信息
     * @param {*} offset 
     *  @param {*} limit 
     */
    async wxaGetStoreList(offset, limit) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/get_store_list?access_token=${access_token}`
        const data = {
            offset: offset,
            limit: limit
        }
        const res = await request.post(uri, data)
        return res
    }
    /**
     * 删除门店
     * @param {*} poi_id 
     */
    async wxaDelStore(poi_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/wxa/del_store?access_token=${access_token}`
        const data = {
            poi_id: poi_id
        }
        const res = await request.post(uri, data)
        return res
    }
}

module.exports = Poi