const request = require('../modules/request')

class ShakeAround {
    constructor() {

    }

    /**
     * ​申请开通功能
     * @param {*} name 联系人姓名，不超过20汉字或40个英文字母
     * @param {*} phone_number 联系人电话
     * @param {*} email 联系人邮箱
     * @param {*} industry_id 平台定义的行业代号，具体请查看链接 行业代号
     * @param {*} qualification_cert_urls 相关资质文件的图片url，图片需先上传至微信侧服务器，用“素材管理-上传图片素材”接口上传图片，返回的图片URL再配置在此处； 当不需要资质文件时，数组内可以不填写url
     * @param {*} apply_reason 申请理由，不超过250汉字或500个英文字母
     */
    async shakeAroundAccountRegister(name, phone_number, email, industry_id, qualification_cert_urls, apply_reason = false) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/account/register?access_token=${access_token}`
        const data = {
            name: name,
            phone_number: phone_number,
            email: email,
            industry_id: industry_id,
            qualification_cert_urls: qualification_cert_urls
        }
        if (apply_reason) {
            data['apply_reason'] = apply_reason
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 查询审核状态
     */
    async shakeAroundAccountAuditStatus() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/account/register?access_token=${access_token}`
        const res = await request.get(uri)
        return res
    }

    /**
     * 申请设备ID
     * @param {*} quantity 	申请的设备ID的数量，单次新增设备超过500个，需走人工审核流程
     * @param {*} apply_reason 申请理由，不超过100个汉字或200个英文字母
     * @param {*} comment 备注，不超过15个汉字或30个英文字母
     * @param {*} poi_id 设备关联的门店ID，关联门店后，在门店1KM的范围内有优先摇出信息的机会。
     */
    async shakeAroundDeviceApplyId(quantity, apply_reason, comment = false, poi_id = false) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const data = {
            quantity: quantity,
            apply_reason: apply_reason,

        }
        if (comment) {
            data['comment'] = comment
        }
        if (poi_id) {
            data['poi_id'] = poi_id
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/applyid?access_token=${access_token}`
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 查询设备ID申请审核状态
     * @param {*} apply_id 
     */
    async shakeAroundDeviceApplyStatus(apply_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/applystatus?access_token=${access_token}`
        const data = {
            apply_id: apply_id,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 编辑设备信息
     * @param {*} device_id 
     * @param {*} uuid 
     * @param {*} major 
     * @param {*} minor 
     * @param {*} comment 
     */
    async shakeAroundDeviceUpdate(device_id, uuid, major, minor, comment) {

        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const data = {
            device_identifier: {
                device_id: device_id,
                uuid: uuid,
                major: major,
                minor: minor
            },
            comment: comment
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/update?access_token=${access_token}`
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 配置设备与门店的关联关系
     * @param {*} device_id 
     * @param {*} uuid 
     * @param {*} major 
     * @param {*} minor 
     * @param {*} poi_id 
     */
    async shakeAroundDeviceBindLocation(device_id, uuid, major, minor, poi_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const data = {
            device_identifier: {
                device_id: device_id,
                uuid: uuid,
                major: major,
                minor: minor
            },
            poi_id: poi_id
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/bindlocation?access_token=${access_token}`
        const res = await request.json(uri, data)
        return res
    }
    /**
     * 配置设备与其他公众账号门店的关联关系
     * @param {*} device_id 
     * @param {*} uuid 
     * @param {*} major 
     * @param {*} minor 
     * @param {*} poi_id 
     * @param {*} type
     * @param {*} poi_appid
     */
    async shakeAroundDeviceBindOtherLocation(device_id, uuid, major, minor, poi_id, type, poi_appid) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const data = {
            device_identifier: {
                device_id: device_id,
                uuid: uuid,
                major: major,
                minor: minor
            },
            poi_id: poi_id,
            type: type,
            poi_appid: poi_appid,
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/bindlocation?access_token=${access_token}`
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 查询设备列表
     * @param {*} device_id 
     * @param {*} uuid 
     * @param {*} major 
     * @param {*} minor 
     * @param {*} poi_id 
     * @param {*} type 
     * @param {*} poi_appid 
     * @param {*} last_seen 
     * @param {*} count 
     */
    async shakeAroundDeviceSearch(device_id, uuid, major, minor, poi_id, type, poi_appid, last_seen, count) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const data = {

            type: type,
            device_identifier: {
                device_id: device_id,
                uuid: uuid,
                major: major,
                minor: minor
            },
        }
        if (type == 2) {
            data['last_seen'] = last_seen
            data['count'] = count
        }
        if (type == 3) {
            data['poi_appid'] = poi_appid
            data['last_seen'] = last_seen
            data['count'] = count
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/search?access_token=${access_token}`
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 页面管理
     * @param {*} title 
     * @param {*} description 
     * @param {*} icon_url 
     * @param {*} comment 
     */
    async shakeAroundPageAdd(title, description, icon_url, page_url, comment = false) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/page/add?access_token=${access_token}`
        const data = {
            title: title,
            description: description,
            page_url: page_url,
            icon_url: icon_url,
        }
        if (comment) {
            data['comment'] = comment
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 编辑页面信息
     * @param {*} page_id 
     * @param {*} title 
     * @param {*} description 
     * @param {*} icon_url 
     * @param {*} comment 
     */
    async shakeAroundPageUpdate(page_id, title, description, icon_url, comment = false) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/page/update?access_token=${access_token}`
        const data = {
            page_id: page_id,
            title: title,
            description: description,
            page_url: page_url,
            icon_url: icon_url,
        }
        if (comment) {
            data['comment'] = comment
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 查询页面列表
     * @param {*} type  查询类型。1： 查询页面id列表中的页面信息；2：分页查询所有页面信息
     * @param {*} page_ids 	指定页面的id列表；当type为1时，此项为必填
     * @param {*} begin 页面列表的起始索引值；当type为2时，此项为必填
     * @param {*} count 待查询的页面数量，不能超过50个；当type为2时，此项为必填
     */
    async shakeAroundPageSearch(type, page_ids, begin, count) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/page/search?access_token=${access_token}`
        const data = {
            type: type
        }
        if (type == 1) {
            data['page_ids'] = page_ids
        }
        if (type == 2) {
            data['begin'] = begin
            data['count'] = count
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 删除页面
     * @param {*} page_id 
     */
    async shakeAroundPageDelete(page_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/page/search?access_token=${access_token}`
        const data = {
            page_id: page_id
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 上传图片素材
     * @param {*} file 图片名字
     * @param {*} type Icon：摇一摇页面展示的icon图；License：申请开通摇一摇周边功能时需上传的资质文件；若不传type，则默认type=icon
     */
    async shakeAroundMaterialAdd(file, type = 'icon') {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/material/add?access_token=${access_token}&type=${type}`
        const data = {
            file: file
        }
        const res = await request.file(uri, data)
        return res
    }

    /**
     * 配置设备与页面的关联关系
     * @param {*} device_id 指定页面的设备ID 设备编号，若填了UUID、major、minor，则可不填设备编号，若二者都填，则以设备编号为优先
     * @param {*} uuid UUID、major、minor，三个信息需填写完整，若填了设备编号，则可不填此信息
     * @param {*} major UUID、major、minor，三个信息需填写完整，若填了设备编号，则可不填此信息
     * @param {*} minor UUID、major、minor，三个信息需填写完整，若填了设备编号，则可不填此信息
     * @param {*} page_ids 待关联的页面列表
     */
    async shakeAroundDeviceBindPage(device_id, uuid, major, minor, page_ids) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/page/update?access_token=${access_token}`
        const data = {
            device_identifier: {
                device_id: device_id,
                uuid: uuid,
                major: major,
                minor: minor
            },
            page_ids: page_ids,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 查询设备与页面的关联关系
     * @param {*} type 查询方式。1： 查询设备的关联关系；2：查询页面的关联关系
     * @param {*} device_id 
     * @param {*} uuid 
     * @param {*} major 
     * @param {*} minor 
     * @param {*} page_id 
     * @param {*} begin 
     * @param {*} count 
     */
    async shakeAroundRelationSearch(type, device_id, uuid, major, minor, page_id, begin, count) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/relation/search?access_token=${access_token}`
        const data = {
            type: type,
        }
        if (type == 1) {
            data['device_identifier'] = {
                device_id: device_id,
                uuid: uuid,
                major: major,
                minor: minor
            }
        }
        if (type == 2) {
            data['page_id'] = page_id,
                data['begin,'] = begin,
                data['count'] = count
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 以设备为维度的数据统计接口
     * @param {*} device_id 
     * @param {*} uuid 
     * @param {*} major 
     * @param {*} minor 
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async shakeAroundStatisticsDevice(device_id, uuid, major, minor, begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/statistics/device?access_token=${access_token}`
        const data = {
            device_identifier: {
                device_id: device_id,
                uuid: uuid,
                major: major,
                minor: minor
            },
            begin_date: begin_date,
            end_date: end_date,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 批量查询设备统计数据接口
     * @param {*} date 指定查询日期时间戳，单位为秒
     * @param {*} page_index 指定查询的结果页序号；返回结果按摇周边人数降序排序，每50条记录为一页
     */
    async shakeAroundStatisticsDeviceList(date, page_index) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/statistics/devicelist?access_token=${access_token}`
        const data = {
            date: date,
            page_index: page_index
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 以页面为维度的数据统计接口
     * @param {*} page_id 
     * @param {*} begin_date 
     * @param {*} end_date 
     */
    async shakeAroundStatisticsPage(page_id, begin_date, end_date) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/statistics/page?access_token=${access_token}`
        const data = {

            page_id: page_id,
            begin_date: begin_date,
            end_date: end_date
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 批量查询页面统计数据接口
     * @param {*} date 
     * @param {*} page_index 
     */
    async shakeAroundStatisticsPageList(date, page_index) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/statistics/pagelist?access_token=${access_token}`
        const data = {

            date: date,
            page_index: page_index,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 新增分组
     * @param {*} group_name 
     */
    async shakeAroundDeviceGroupAdd(group_name) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/group/add?access_token=${access_token}`
        const data = {

            group_name: group_name,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 编辑分组信息
     * @param {*} group_id 
     * @param {*} group_name 
     */
    async shakeAroundDeviceGroupUpdate(group_id, group_name) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/group/update?access_token=${access_token}`
        const data = {
            group_id: group_id,
            group_name: group_name,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 删除分组
     * @param {*} group_id 
     */
    async shakeAroundDeviceGroupDelete(group_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/group/delete?access_token=${access_token}`
        const data = {
            group_name: group_name,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 查询分组列表
     * @param {*} begin 
     * @param {*} count 
     */
    async shakeAroundDeviceGroupGetList(begin, count) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/group/getlist?access_token=${access_token}`
        const data = {
            begin: begin,
            count: count,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 查询分组详情
     * @param {*} group_id 
     * @param {*} begin 
     * @param {*} count 
     */
    async shakeAroundDeviceGroupGetDetail(group_id, begin, count) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/group/getdetail?access_token=${access_token}`
        const data = {
            begin: begin,
            count: count,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 添加设备到分组
     * @param {*} group_id 
     * @param {*} device_id 
     * @param {*} uuid 
     * @param {*} major 
     * @param {*} minor 
     */
    async shakeAroundDeviceGroupAddDevice(group_id, device_id, uuid, major, minor) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const data = {
            group_id: group_id,
            device_identifier: {
                device_id: device_id,
                uuid: uuid,
                major: major,
                minor: minor
            },
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/group/adddevice?access_token=${access_token}`
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 从分组中移除设备
     * @param {*} group_id 
     * @param {*} device_id 
     * @param {*} uuid 
     * @param {*} major 
     * @param {*} minor 
     */
    async shakeAroundDeviceGroupDeleteDevice(group_id, device_id, uuid, major, minor) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const data = {
            group_id: group_id,
            device_identifier: {
                device_id: device_id,
                uuid: uuid,
                major: major,
                minor: minor
            },
        }
        const uri = `https://api.weixin.qq.com/shakearound/device/group/deletedevice?access_token=${access_token}`
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取设备及用户信息
     * @param {*} ticket 
     * @param {*} need_poi 
     */
    async shakeAroundUserGetShakeInfo(ticket, need_poi) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const data = {
            ticket: ticket
        }
        if (need_poi) {
            data['need_poi'] = need_poi
        }
        const uri = `https://api.weixin.qq.com/shakearound/user/getshakeinfo?access_token=${access_token}`
        const res = await request.json(uri, data)
        return res
    }

    /**
     * TODO 红包预下单接口
     */
    async mmpaymkTtransfersBbPreOrder() {

    }

    /**
     * 创建红包活动
     * @param {int} use_template 	是否使用模板，1：使用，2：不使用,以参数的形式拼装在url后。（模版即交互流程图中的红包加载页，使用模板用户不需要点击可自动打开红包；不使用模版需自行开发HTML5页面，并在页面调用红包jsapi）
     * @param {string} logo_url 	使用模板页面的logo_url，不使用模板时可不加。展示在摇一摇界面的消息图标。图片尺寸为120x120。
     * @param {string} title 	抽奖活动名称（选择使用模板时，也作为摇一摇消息主标题），最长6个汉字，12个英文字母。
     * @param {string} desc 	抽奖活动描述（选择使用模板时，也作为摇一摇消息副标题），最长7个汉字，14个英文字母。
     * @param {int} onoff 	抽奖开关。0关闭，1开启，默认为1
     * @param {long} begin_time 	抽奖活动开始时间，unix时间戳，单位秒
     * @param {long} expire_time 	抽奖活动结束时间，unix时间戳，单位秒
     * @param {string} sponsor_appid 	红包提供商户公众号的appid，需与预下单中的公众账号appid（wxappid）一致
     * @param {long} total 	红包总数，红包总数是录入红包ticket总数的上限，因此红包总数应该大于等于预下单时红包ticket总数。
     * @param {string} jump_url 	红包关注界面后可以跳转到第三方自定义的页面
     * @param {string} key 	开发者自定义的key，用来生成活动抽奖接口的签名参数，长度32位。使用方式见sign生成规则
     */
    async shakeAroundLotteryAddLotteryInfo(use_template, logo_url, title, desc, onoff, begin_time, expire_time, sponsor_appid, total, jump_url, key) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/lottery/setprizebucket?access_token=${access_token}&use_template=${use_template}&logo_url=${logo_url}`
        const data = {
            title: title,
            desc: desc,
            onoff: onoff,
            begin_time: begin_time,
            expire_time: expire_time,
            sponsor_appid: sponsor_appid,
            total: total,
            jump_url: jump_url,
            key: key,
        }
        const res = await request.json(uri, data)
        return res
    }


    /**
     * 录入红包信息
     * @param {String} lottery_id 红包抽奖id，来自addlotteryinfo返回的lottery_id
     * @param {String} mchid 
     * @param {String} sponsor_appid 
     * @param {JSON} prize_info_list 
     */
    async shakeAroundLotterySetPrizeBucket(lottery_id, mchid, sponsor_appid, prize_info_list) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/lottery/setprizebucket?access_token=${access_token}`
        const data = {
            lottery_id: xxxxxxllllll,
            mchid: 10000098,
            sponsor_appid: wx8888888888888888,
            prize_info_list: prize_info_list
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 设置红包活动抽奖开关
     * @param {String} lottery_id 红包抽奖id，来自addlotteryinfo返回的lottery_id
     * @param {Int} onoff 活动抽奖开关，0：关闭，1：开启
     */
    async shakeAroundLotterySetLotterySwitch(lottery_id, onoff) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/lottery/setlotteryswitch?access_token=${access_token}&lottery_id=${lottery_id}&onoff=${onoff}`
        const res = await request.get(uri)
        return res
    }

    /**
     * 红包查询接口
     * @param {String} lottery_id 	红包抽奖id，来自addlotteryinfo返回的lottery_id
     */
    async shakeAroundLotteryQueryLottery(lottery_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/shakearound/lottery/querylottery?access_token=${access_token}&lottery_id=${lottery_id}`
        const res = await request.get(uri)
        return res
    }
}

module.exports = ShakeAround