/**
 * @author [siwi]
 * @email [siwi@siwi.me]
 * @create date 2018-03-15 05:53:13
 * @modify date 2018-03-15 05:53:13
 * @desc [siwi-wechat comment.js]
 */
const request = require('../modules/request')
class Comment {
    constructor() {

    }
    /**
     *  打开已群发文章评论
     * https://api.weixin.qq.com/cgi-bin/comment/open?access_token=ACCESS_TOKEN
     * @param {*} msg_data_id  群发返回的msg_data_id
     * @param {*} index   多图文时，用来指定第几篇图文，从0开始，不带默认操作该msg_data_id的第一篇图文
     */
    async commentOpen(msg_data_id, index = 0) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/comment/open?access_token=${access_token}`
        const data = {
            msg_data_id: msg_data_id,
            index: index
        }
        const res = await request.post(uri, data)
        return res
    }

    /**
     *  关闭已群发文章评论
     * https://api.weixin.qq.com/cgi-bin/comment/close?access_token=ACCESS_TOKEN
     * @param {*} msg_data_id  群发返回的msg_data_id
     * @param {*} index   多图文时，用来指定第几篇图文，从0开始，不带默认操作该msg_data_id的第一篇图文
     */
    async commentClose(msg_data_id, index) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/comment/close?access_token=${access_token}`
        const data = {
            msg_data_id: msg_data_id,
            index: index
        }
        const res = await request.post(uri, data)
        return res
    }

    /**
     * 查看指定文章的评论数据
     * https://api.weixin.qq.com/cgi-bin/comment/list?access_token=ACCESS_TOKEN
     * @param {*} msg_data_id 
     * @param {*} index 
     * @param {*} begin 
     * @param {*} count 	获取数目（>=50会被拒绝）
     * @param {*} type type=0 普通评论&精选评论 type=1 普通评论 type=2 精选评论
     */
    async commentList(msg_data_id, index=0, begin, count, type) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/comment/list?access_token=${access_token}`
        const data = {
            msg_data_id: msg_data_id,
            index: index,
            begin: begin,
            count: count,
            type: type,
        }
        const res = await request.post(uri, data)
        return res
    }

    /**
     * 将评论标记精选（新增接口）
     * https://api.weixin.qq.com/cgi-bin/comment/markelect?access_token=ACCESS_TOKEN
     * @param {*} msg_data_id 
     * @param {*} index 
     * @param {*} user_comment_id 
     */
    async commentMarkelect(msg_data_id, index, user_comment_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/comment/markelect?access_token=${access_token}`
        const data = {
            access_token: access_token,
            msg_data_id: msg_data_id,
            index: index,
            user_comment_id: user_comment_id,
        }
        const res = await request.post(uri, data)
        return res
    }

    /**
     * 将评论取消精选（新增接口）
     * https://api.weixin.qq.com/cgi-bin/comment/unmarkelect?access_token=ACCESS_TOKEN
     * @param {*} msg_data_id 
     * @param {*} index 
     * @param {*} user_comment_id 
     */
    async commentUnMarkelect(msg_data_id, index, user_comment_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/comment/unmarkelect?access_token=${access_token}`
        const data = {
            access_token: access_token,
            msg_data_id: msg_data_id,
            index: index,
            user_comment_id: user_comment_id,
        }
        const res = await request.post(uri, data)
        return res
    }
    /**
     *  删除评论
     * https://api.weixin.qq.com/cgi-bin/comment/delete?access_token=ACCESS_TOKEN
     * @param {*} msg_data_id 
     * @param {*} index 
     * @param {*} user_comment_id 
     */
    async commentDelete(msg_data_id, index, user_comment_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/comment/delete?access_token=${access_token}`
        const data = {
            access_token: access_token,
            msg_data_id: msg_data_id,
            index: index,
            user_comment_id: user_comment_id,
        }
        const res = await request.post(uri, data)
        return res
    }
    /**
     *  回复评论
     * https://api.weixin.qq.com/cgi-bin/comment/delete?access_token=ACCESS_TOKEN
     * @param {*} msg_data_id 
     * @param {*} index 
     * @param {*} user_comment_id 
     */
    async commentReplyAdd(msg_data_id, index, user_comment_id, content) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/comment/reply/add?access_token=${access_token}`
        const data = {
            access_token: access_token,
            msg_data_id: msg_data_id,
            index: index,
            user_comment_id: user_comment_id,
            content: content,
        }
        const res = await request.post(uri, data)
        return res
    }

    /**
     *  删除回复
     * https://api.weixin.qq.com/cgi-bin/comment/reply/delete?access_token=ACCESS_TOKEN
     * @param {*} msg_data_id 
     * @param {*} index 
     * @param {*} user_comment_id 
     */
    async commentReplyDelete(msg_data_id, index, user_comment_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/comment/reply/delete?access_token=${access_token}`
        const data = {
            access_token: access_token,
            msg_data_id: msg_data_id,
            index: index,
            user_comment_id: user_comment_id,
        }
        const res = await request.post(uri, data)
        return res
    }
    
}

module.exports = Comment