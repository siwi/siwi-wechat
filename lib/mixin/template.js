const request = require('../modules/request')
class Template {
    constructor() {}

    /**
     * 设置所属行业
     * @param {*} industry_id1 
     * @param {*} industry_id2 
     */
    async templateApiSetIndustry(industry_id1, industry_id2) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=${access_token}`
        const data = {
            industry_id1: industry_id1,
            industry_id2: industry_id2
        }

        const res = await request.json(uri, data)
        return res
    }
    /**
     * 获取设置的行业信息
     * https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=ACCESS_TOKEN
     */
    async templateGetIndustry() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=${access_token}`
        
        const res = await request.get(uri)
        return res
    }

    /**
     * 获得模板ID
     * @param {*} template_id_short 
     */
    async templateApiAddTemplate(template_id_short) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token=${access_token}`
        const data = {
            template_id_short: template_id_short
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取模板列表
     */
    async templateGetAllPrivateTemplate() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token=${access_token}`
        const res = await request.get(uri)
        return res
    }

    /**
     * 删除模板
     * @param {*} template_id 
     */
    async templateDelPrivateTemplate(template_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/template/del_private_template?access_token=${access_token}`
        const data = {
            template_id: template_id
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 发送模板消息
     * https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN
     *      参数	        是否必填	            说明
            touser	        是	            接收者openid
            template_id	    是	            模板ID
            url	            否	            模板跳转链接
            miniprogram	    否	            跳小程序所需数据，不需跳小程序可不用传该数据
            appid	        是	            所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系）
            pagepath	    是	            所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar）
            data	        是	            模板数据
            color	        否	            模板内容字体颜色，不填默认为黑色
     */
    async templateSend(data) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=${access_token}`
        const res = await request.json(uri, data)
        return res
    }
}

module.exports = Template