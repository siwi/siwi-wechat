const request = require('../modules/request')
class Card {
    constructor() {}


    /**
     * 创建卡券
     * @param {*} data 
     */
    async cardCreate(data) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/create?access_token=${access_token}`
        const data = data

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 设置买单接口
     * @param {*} card_id 
     * @param {*} is_open 
     */
    async cardPayCellSet(card_id, is_open) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/paycell/set?access_token=${access_token}`
        const data = {
            access_token: access_token,
            card_id: card_id,
            is_open: is_open
        }
        const res = await request.json(uri, data)
        return res
    }
    /**
     * 设置自助核销接口
     * @param {*} card_id 
     * @param {*} is_open 
     */
    async cardSelfConsumeCellSet(card_id, is_open = false, need_verify_cod = false, need_remark_amount = false) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/selfconsumecell/set?access_token=${access_token}`
        const data = {
            access_token: access_token,
            card_id: card_id,
            is_open: is_open,
            need_verify_cod: need_verify_cod,
            need_remark_amount: need_remark_amount
        }
        const res = await request.json(uri, data)
        return res
    }


    /**
     * 创建二维码接口
     * @param {*} json 
     */
    async cardQrcodeCreate(json) {

        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/qrcode/create?access_token=${access_token}`
        const data = Object.assign({
            access_token: access_token
        }, json)
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 创建货架接口
     * @param {*} json 
     */
    async cardLandingPageCreate(json) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/qrcode/create?access_token=${access_token}`
        const data = Object.assign({
            access_token: access_token
        }, json)
        const res = await request.json(uri, data)
        return res
    }

    async cardCodeDeposit(json) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `http://api.weixin.qq.com/card/code/deposit?access_token=${access_token}`
        const data = Object.assign({
            access_token: access_token
        }, json)
        const res = await request.json(uri, data)
        return res
    }

    /**
     *  查询导入code数目接口
     * @param {*} json 
     */
    async cardCodeGetDepositCount(card_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `http://api.weixin.qq.com/card/code/getdepositcount?access_token=${access_token}`
        const data = {
            access_token: access_token,
            card_id: card_id
        }
        const res = await request.post(uri, data)
        return res
    }

    /**
     * 核查code接口
     * @param {String} card_id 进行导入code的卡券ID
     * @param {Array} code  已经微信卡券后台的自定义code，上限为100个
     */
    async cardCodeCheckCode(card_id, code) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `http://api.weixin.qq.com/card/code/getdepositcount?access_token=${access_token}`
        const data = {
            access_token: access_token,
            card_id: card_id,
            code: code
        }
        const res = await request.post(uri, data)
        return res
    }

    /**
     * 图文消息群发卡券
     * @param {*} card_id 
     */
    async cardMpNewsGetHtml(card_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/mpnews/gethtml?access_token=${access_token}`
        const data = {
            access_token: access_token,
            card_id: card_id,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 设置测试白名单
     * @param {*} json 
     */
    async cardTestWhiteListSet(json) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/testwhitelist/set?access_token=${access_token}`
        const data = Object.assign({
            access_token: access_token
        }, json)
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 查询Code接口
     * @param {*} json 
     */
    async cardCodeGet(card_id, code, check_consume) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/code/get?access_token=${access_token}`
        const data = {
            access_token: access_token,
            card_id: card_id,
            code: code,
            check_consume: check_consume,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 核销Code接口
     * @param {*} json 
     */
    async cardCodeConsume(code, card_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/code/consume?access_token=${access_token}`
        const data = {
            access_token: access_token,
            code: code,
        }
        if (card_id) {
            data['card_id'] = card_id
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * Code解码接口
     * @param {*} encrypt_code 
     */
    async cardCodeDecrypt(encrypt_code) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/code/decrypt?access_token=${access_token}`
        const data = {
            access_token: access_token,
            encrypt_code: encrypt_code,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取用户已领取卡券接口
     * @param {*} openid 
     * @param {*} card_id 
     */
    async cardUserGetCardList(openid, card_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/user/getcardlist?access_token=${access_token}`
        const data = {
            access_token: access_token,
            encrypt_code: encrypt_code,
        }
        const res = await request.json(uri, data)
        return res
    }
    /**
     * 查看卡券详情
     * @param {*} card_id 
     */
    async cardGet(card_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/user/getcardlist?access_token=${access_token}`
        const data = {
            access_token: access_token,
            card_id: card_id,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 批量查询卡券列表
     * @param {*} card_id 
     */
    async cardBatchGet(offset, count, status_list = false) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/user/batchget?access_token=${access_token}`
        const data = {
            offset: offset,
            count: count,
        }
        if (status_list) {
            data['status_list'] = status_list
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 更改卡券信息接口
     * @param {*} json 
     */
    async cardUpdate(json) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/update?access_token=${access_token}`
        const data = Object.assign({
            access_token: access_token
        }, json)
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 修改库存接口
     * @param {*} card_id 
     * @param {*} increase_stock_value 
     * @param {*} reduce_stock_value 
     */
    async cardModifyStock(card_id, increase_stock_value, reduce_stock_value) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/update?access_token=${access_token}`
        const data = {
            access_token: access_token,
            card_id: card_id,
        }
        if (increase_stock_value) {
            data['increase_stock_value'] = increase_stock_value
        }
        if (reduce_stock_value) {
            data['reduce_stock_value'] = reduce_stock_value
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 更改Code接口
     * @param {*} code 
     * @param {*} card_id 
     * @param {*} new_code 
     */
    async cardCodeUpdate(code, card_id, new_code) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/code/update?access_token=${access_token}`
        const data = {
            access_token: access_token,
            card_id: card_id,
            code: code,
            new_code: new_code
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 删除卡券接口
     * @param {*} card_id 
     */
    async cardDelete(card_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/delete?access_token=${access_token}`
        const data = {
            access_token: access_token,
            card_id: card_id,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 设置卡券失效接口
     * @param {*} card_id 
     * @param {*} code 
     * @param {*} reason 
     */
    async cardCodeUnAvailable(card_id, code, reason = false) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/card/code/unavailable?access_token=${access_token}`
        const data = {
            access_token: access_token,
            card_id: card_id,
            code: code
        }
        if (reason) {
            data['reason'] = reason
        }
        const res = await request.json(uri, data)
        return res
    }
}

module.exports = Card