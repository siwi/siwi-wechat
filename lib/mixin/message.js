const request = require('../modules/request') 
class Message {
    constructor () {

    }
    /**
     * 获取微信服务器IP地址
     * https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=ACCESS_TOKEN
     */
    async getCallbackIp() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=${access_token}`
        const res = await request.get(uri, data)
        return res
    }
    
    /**
     * 公众号调用或第三方平台帮公众号调用对公众号的所有api调用（包括第三方帮其调用）次数进行清零：
     * https://api.weixin.qq.com/cgi-bin/clear_quota?access_token=ACCESS_TOKEN
     * @param {String} appid  公众号的APPID
     */
    async clearQuota(appid) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/clear_quota?access_token=${access_token}`
        const data = {
            access_token: access_token,
            appid: appid
        }
        const res = await request.post(uri, data)
        return res
    }
    /**
     * 获取公众号的自动回复规则
     * https://api.weixin.qq.com/cgi-bin/get_current_autoreply_info?access_token=ACCESS_TOKEN
     */
    async getCurrentAutoReplyInfo    () {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/get_current_autoreply_info?access_token=${access_token}`
        const res = await request.get(uri)
        return res
    }
}

module.exports = Message