const request = require('../modules/request')
class Mass {
    constructor() {}
    /**
     *  根据标签进行群发 
     */
    async messageMassSendAll(json) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=${access_token}`
        const data = Object.assign({
                access_token: access_token,
            },
            json
        )
        const res = request.post(uri, data)
        return res
    }

    /**
     * 
     */
    async messageMassSend() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=${access_token}`
        const data = {
            msg_id: msg_id,
            article_idx: article_idx
        }
        const res = request.post(uri, data)
        return res
    }

    /**
     * 删除群发
     * https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=ACCESS_TOKEN
     * @param {*} msg_id 
     * @param {*} article_idx 
     */
    async messageMassDelete(msg_id, article_idx) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=${access_token}`
        const data = {
            msg_id: msg_id,
            article_idx: article_idx
        }
        const res = request.post(uri, data)
        return res
    }

    /**
     * 
     */
    async messageMassPreview() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=${access_token}`
        const data = {
            msg_id: msg_id,
            article_idx: article_idx
        }
        const res = request.post(uri, data)
        return res
    }

    /**
     * 
     * @param {*} speed 
     */
    async messageMassSpeedSet(speed) {
        // https://api.weixin.qq.com/cgi-bin/message/mass/speed/set?access_token=ACCESS_TOKEN
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/message/mass/speed/set?access_token=${access_token}`
        const data = {
            access_token: access_token,
            speed: speed
        }
        const res = request.post(uri, data)
        return res
    }
}

module.exports = Mass