const request = require('../modules/request') 
class Menu {
    constructor () {

    }

    /**
     * 自定义菜单接
     * https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN
     * @param {*} button 
     *  参数	             是否必须	                                    说明
        button	                是	                        一级菜单数组，个数应为1~3个
        sub_button	            否	                        二级菜单数组，个数应为1~5个
        type	                是	                        菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
        name	                是	                        菜单标题，不超过16个字节，子菜单不超过60个字节
        key	              click等点击类型必须	              菜单KEY值，用于消息接口推送，不超过128字节
        url	            view、miniprogram类型必须	         网页 链接，用户点击菜单可打开链接，不超过1024字节。 type为miniprogram时，不支持小程序的老版本客户端将打开本url。
        media_id	media_id类型和view_limited类型必须	      调用新增永久素材接口返回的合法media_id
        appid	    miniprogram类型必须	                     小程序的appid（仅认证公众号可配置）
        pagepath	miniprogram类型必须	                     小程序的页面路径
     */
    async menuCreate(button) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/menu/create?access_token=${access_token}`
        const data = {
            button: button,
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 自定义菜单查询接口
     * https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN
     */
    async menuGet() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/menu/get?access_token=${access_token}`
        const data = {
            access_token: access_token,
        }

        const res = await request.get(uri, data)
        return res
    }

    /**
     * 自定义菜单删除接口
     * https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN 
     */
    async menuDelete() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=${access_token}`
        
        const res = await request.get(uri)
        return res
    }

    /**
     * 创建个性化菜单
     * https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=ACCESS_TOKEN
     * @param {菜单} button 
     * @param {菜单匹配规则} matchrule 
     * 
        {
            "button":[
            {    
                "type":"click",
                "name":"今日歌曲",
                "key":"V1001_TODAY_MUSIC" },
            {     "name":"菜单",
                "sub_button":[
                {            
                    "type":"view",
                    "name":"搜索",
                    "url":"http://www.soso.com/"},
                    {
                                "type":"miniprogram",
                                "name":"wxa",
                                "url":"http://mp.weixin.qq.com",
                                "appid":"wx286b93c14bbf93aa",
                                "pagepath":"pages/lunar/index"
                    },
                    {
                "type":"click",
                "name":"赞一下我们",
                "key":"V1001_GOOD"
                }]
            }],
            "matchrule":{
                "tag_id":"2",
                "sex":"1",
                "country":"中国",
                "province":"广东",
                "city":"广州",
                "client_platform_type":"2",
                "language":"zh_CN"
            }
        }
     */
    async menuAddConditional(button, matchrule) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        
        const uri = `https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=${access_token}`
        const data = {
            button: button,
            matchrule: matchrule
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 删除个性化菜单
     * https://api.weixin.qq.com/cgi-bin/menu/delconditional?access_token=ACCESS_TOKEN 
     * @param {*} menuid 
     */
    async menuDelConditional(menuid) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/menu/delconditional?access_token=${access_token}`
        const data = {
            menuid: menuid,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 测试个性化菜单匹配结果
     * https://api.weixin.qq.com/cgi-bin/menu/trymatch?access_token=ACCESS_TOKEN
     * @param {可以是粉丝的OpenID，也可以是粉丝的微信号} user_id 
     */
    async menuTryMatch(user_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/menu/trymatch?access_token=${access_token}`
        const data = {
            access_token: access_token,
            user_id: user_id,
        }
        const res = await request.json(uri, data)
        return res
    }
    
    /**
     * 获取自定义菜单配置接口
     * https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=ACCESS_TOKEN
     */
    async getCurrentSelfMenuInfo() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info`
        const data = {
            access_token: access_token
        }
        const res = await request.get(uri, data)
        return res
    }

}

module.exports = Menu