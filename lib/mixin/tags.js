const request = require('../modules/request')
class Tags {
    constructor() {

    }
    /**
     * 创建标签
     * https://api.weixin.qq.com/cgi-bin/tags/create?access_token=ACCESS_TOKEN
     * @param {name} name
     */
    async tagsCreate(name) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/tags/create?access_token=${access_token}`
        const data = {
            tag: {
                name: name
            }
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取公众号已创建的标签
     * https://api.weixin.qq.com/cgi-bin/tags/get?access_token=ACCESS_TOKEN 
     */
    async tagsGet() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/tags/get?access_token=${access_token}`
        
        const res = await request.get(uri)
        return res
    }

    /**
     * 编辑标签
     * https://api.weixin.qq.com/cgi-bin/tags/update?access_token=ACCESS_TOKEN
     * @param {Object} tag 
     */
    async tagsUpdate(tag) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/tags/update?access_token=${access_token}`
        const data = {
            access_token: access_token,
            tag: {
                id: tag['id'],
                name: tag['name']
            }
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 删除标签 当某个标签下的粉丝超过10w时，后台不可直接删除标签。此时，开发者可以对该标签下的openid列表，先进行取消标签的操作，直到粉丝数不超过10w后，才可直接删除该标签
     * https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=ACCESS_TOKEN
     * @param {Number} tag_id 标签id
     */
    async tagsDelete(tag_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=${access_token}`
        const data = {
            access_token: access_token,
            tag: {
                'id': tag_id
            }
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取标签下粉丝列表
     * https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=ACCESS_TOKEN
     * @param {*} tag_id 
     * @param {*} next_openid 
     */
    async userTagGet(tag_id, next_openid='') {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=${access_token}`
        const data = {
            tagid: tag_id,
            next_openid: next_openid
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 批量为用户打标签
     * https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=ACCESS_TOKEN
     * @param {Array} openid_list  粉丝列表
     * @param {Number} tag_id 
     */
    async tagsMembersBatchTagging(openid_list, tag_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=${access_token}`
        const data = {
            openid_list: openid_list,
            tagid: tag_id
        }
        const res = await request.post(uri, data)
        return res
    }

    /**
     * 批量为用户取消标签
     * https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=ACCESS_TOKEN
     * @param {Array} openid_list  粉丝列表
     * @param {Number} tag_id 
     */
    async tagsMembersBatchUnTagging(openid_list, tag_id) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=${access_token}`
        const data = {
            openid_list: openid_list,
            tagid: tag_id
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取用户身上的标签列表
     */
    async tagsGetIdList(openid) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token=${access_token}`
        const data = {
            openid: openid
        }
        
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取公众号的黑名单列表
     * https://api.weixin.qq.com/cgi-bin/tags/members/getblacklist?access_token=ACCESS_TOKEN
     * @param {*} begin_openid 
     */
    async tagsMembersGetBlackList(begin_openid ='') {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/tags/members/getblacklist?access_token=${access_token}`
        const data = {
            begin_openid: begin_openid,
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 拉黑用户
     * @param {*} openid_list 
     */
    async tagsMembersBatchBlackList(openid_list) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/tags/members/batchblacklist?access_token=${access_token}`
        const data = {
            openid_list: openid_list,
        }

        const res = await request.json(uri, data)
        return res
    }

    /**
     * 取消拉黑用户
     * @param {*} openid_list 
     */
    async tagsMembersBatchUnBlackList(openid_list) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/tags/members/batchunblacklist?access_token=${access_token}`
        const data = {
            access_token: access_token,
            openid_list: openid_list,
        }

        const res = await request.post(uri, data)
        return res
    }
}

module.exports = Tags