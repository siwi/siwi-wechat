const request = require('../modules/request')

class Scan {
    constructor() {

    }

    /**
     * 获取商户信息
     * https://api.weixin.qq.com/scan/merchantinfo/get?access_token=TOKEN 
     */
    async scanMerchantInfoGet() {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/scan/merchantinfo/get?access_token=${access_token}`
        const res = await request.get(uri)
        return res
    }

    /**
     * 创建商品
     * https://api.weixin.qq.com/scan/product/create?access_token=TOKEN
     */
    async scanProductCreate(json) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/scan/product/create?access_token=${access_token}`
        const data = json
        const res = await request.json(uri, data)
        return res
    }


    /**
     * 商品发布
     * https://api.weixin.qq.com/scan/product/create?access_token=TOKEN
     * @param {*} keystandard 
     * @param {*} keystr 
     * @param {*} status 
     */
    async scanProductModStatus(keystandard, keystr, status) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/scan/product/modstatus?access_token=${access_token}`
        const data = {
            access_token: access_token,
            keystandard: keystandard,
            keystr: keystr,
            status: status
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 设置测试人员白名单
     * https://api.weixin.qq.com/scan/testwhitelist/set?access_token=TOKEN
     * @param {*} openid 
     * @param {*} username 
     */
    async scanTestWhiteListSet(openid, username) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/scan/testwhitelist/set?access_token=${access_token}`
        const data = {
            access_token: access_token,
            openid: openid,
            username: username,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 获取商品二维码
     * @param {*} keystandard  商品编码标准。
     * @param {*} keystr  商品编码内容。
     * @param {*} qrcode_size 二维码的尺寸（整型），数值代表边长像素数，不填写默认值为100。
     * @param {*} extinfo 由商户自定义传入，建议仅使用大小写字母、数字及-_().*这6个常用字符。
     */
    async scanProductGetQrcode(keystandard, keystr, qrcode_size, extinfo) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/scan/product/getqrcode?access_token=${access_token}`
        const data = {
            access_token: access_token,
            keystandard: keystandard,
            keystr: keystr,
            extinfo: extinfo,
            qrcode_size: qrcode_size
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 查询商品信息
     * @param {*} keystandard 	商品编码标准。
     * @param {*} keystr 商品编码内容。
     */
    async scanProductGet(keystandard, keystr) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/scan/product/get?access_token=${access_token}`
        const data = {
            access_token: access_token,
            keystandard: keystandard,
            keystr: keystr,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 批量查询商品信息
     * @param {*} offset 
     * @param {*} limit 
     * @param {*} status 
     * @param {*} keystr 
     */
    async scanProductGetList(offset, limit, status, keystr) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/scan/product/getlist?access_token=${access_token}`
        const data = {
            access_token: access_token,
            keystandard: keystandard,
            keystr: keystr,
        }
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 更新商品信息
     * @param {*} json 
     */
    async scanProductUpdate(json) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/scan/product/update?access_token=${access_token}`
        const data = Object.assign({
            access_token: access_token
        }, json)
        const res = await request.json(uri, data)
        return res
    }
    /**
     * 检查wxticket参数
     * @param {*} keystandard 	商品编码标准。
     * @param {*} keystr 商品编码内容。
     */
    async scanProductGet(keystandard, keystr) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/scan/scanticket/check?access_token=${access_token}`
        const data = {
            access_token: access_token,
            ticket: ticket,
        }
        const res = await request.post(uri, data)
        return res
    }

    /**
     * 清除商品信息
     * @param {*} ticket 	商品编码标准。
     */
    async scanTicketCheck(ticket) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/scan/product/clear?access_token=${access_token}`
        const data = {
            access_token: access_token,
            keystandard: keystandard,
            keystr: keystr,
        }
        const res = await request.json(uri, data)
        return res
    }


    /**
     * 清除扫码记录
     * @param {*} keystandard 
     * @param {*} keystr 
     * @param {*} extinfo 
     */
    async scanScanTicketCheck(keystandard, keystr, extinfo) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `https://api.weixin.qq.com/scan/scanticket/check?access_token=${access_token}`
        const data = {
            access_token: access_token,
            keystandard: keystandard,
            keystr: keystr,
            extinfo: extinfo
        }
        const res = await request.json(uri, data)
        return res
    }

}

module.exports = Scan