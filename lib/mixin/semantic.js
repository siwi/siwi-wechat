const request = require('../modules/request')

class Semantic {
    constructor() {
        
    }

    /**
     * 语义理解
     * @param {*} query 
     * @param {*} category 
     * @param {*} appid 
     * @param {*} uid 
     * @param {*} city 
     * @param {*} latitude 
     * @param {*} longitude 
     * @param {*} region 
     */
    async semanticSemproxySearch(query, category,appid, uid,city, latitude, longitude, region) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const data = {
            query: query,
            category: category,
            city: city,
            region: region,
            appid: appid,
            uid: uid,
        }
        if (!city) {
            data['latitude'] = latitude
            data['longitude'] = longitude
        }
        const uri = `https://api.weixin.qq.com/semantic/semproxy/search?access_token=${access_token}`
        const res = await request.json(uri, data)
        return res
    }

    /**
     * 提交语音
     * @param {*} format 
     * @param {*} voice_id 
     * @param {*} lang 
     */
    async mediaVoiceAddVoiceToRecoForText(format, voice_id, lang = 'zh_CN') {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const data = {
            access_token: access_token,
            voice_id: voice_id
        }
        const uri = `http://api.weixin.qq.com/cgi-bin/media/voice/addvoicetorecofortext?access_token=${access_token}&format=${format}&voice_id=${voice_id}&lang=${lang}`
        if (lang) {
            data['lang'] = lang
        }
        const res = await request.post(uri, data)
        return res
    }

    /**
     * 获取语音识别结果
     * @param {*} voice_id 	语音唯一标识
     * @param {*} lang 语言，zh_CN 或 en_US，默认中文
     */
    async mediaVoiceQueryRecoResultForText(voice_id, lang = 'zh_CN') {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `http://api.weixin.qq.com/cgi-bin/media/voice/queryrecoresultfortext?access_token=${access_token}&voice_id=${voice_id}&lang=${lang}`
        const data = {
            access_token: access_token,
            voice_id: voice_id
        }
        if (lang) {
            data['lang'] = lang
        }

        const res = await request.post(uri, data)
        return res
    }

    /**
     * 微信翻译
     * @param {*} body 是	内容
     * @param {*} lfrom 是	源语言，zh_CN 或 en_US
     * @param {*} lto 是	目标语言，zh_CN 或 en_US
     */
    async mediaVoiceTranslateContent(body, lfrom, lto) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }
        const uri = `http://api.weixin.qq.com/cgi-bin/media/voice/translatecontent?access_token=${access_token}&lfrom=${lfrom}}&lto=${lto}`
        const data = {
            body: body
        }
        const res = await request.post(uri, data)
        return res
    }
}

module.exports = Semantic