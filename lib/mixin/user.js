const request = require('../modules/request') 
class User {
    constructor () {

    }

    /**
     * 获取用户基本信息(UnionID机制)
     * @param {*} openid 
     * @param {*} lang 
     */
    async userInfo(openid, lang='zh_CN') {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/user/info?access_token=${access_token}&openid=${openid}&lang=${lang}`

        const res = await request.get(uri)
        return res
    }

    /**
     * 批量获取用户基本信息
     * @param {*} user_list  [{"openid": "otvxTs4dckWG7imySrJd6jSi0CWE", "lang": "zh_CN"}]
     */
    async userInfoBatchGet (user_list) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=${access_token}`
        const data = {
            user_list: user_list
        }
        const res = await request.json(uri, data)
        return res
    }
    /**
     * 获取用户列表
     * @param {*} next_openid 
     */
    async userGet(next_openid=false) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/user/get?access_token=${access_token}&next_openid=${next_openid ? next_openid:''}`

        const res = await request.get(uri)
        return res
    }

    /**
     * 设置用户备注名
     * @param {*} openid 用户标识
     * @param {*} remark 新的备注名，长度必须小于30字符
     */
    async userInfoUpdateRemark(openid, remark) {
        const access_token = await this.getAccessToken()
        if (!access_token) {
            return false
        }

        const uri = `https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=${access_token}`
        const data = {
            access_token: access_token,
            openid: openid,
            remark: remark
        }

        const res = await request.json(uri, data)
        return res
    }
}

module.exports = User