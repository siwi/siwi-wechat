/**
 * @author [siwi]
 * @email [siwi@siwi.me]
 * @create date 2018-03-13 12:52:39
 * @modify date 2018-03-13 12:52:39
 * @desc [生成消息]
 */
let instance = null
class Xml {
    constructor() {

        if (!instance) {
            instance = this
        }
        return instance
    }
    /**
     * 创建消息xml
     * @param {消息类型} msgType 
     */
    async createXml(msgType, data) {
        const common = `
        <xml>
            <ToUserName><![CDATA[${data['toUser']}]]></ToUserName>
            <FromUserName><![CDATA[${data['fromUser']}]]></FromUserName>
            <CreateTime>${Math.floor(Date.now() / 1000)}</CreateTime>
            <MsgType><![CDATA[${msgType}]]></MsgType>`

        let content

        switch (msgType) {
            case 'text':
                content = await this.text(data)
                break;
            case 'image':
                content = await this.image(data)
                break;
            case 'voice':
                content = await this.voice(data)
                break;
            case 'video':
                content = await this.video(data)
                break;
            case 'music':
                content = await this.music(data)
                break;
            case 'news':
                content = await this.news(data)
                break;
            default:
                break;
        }
        return `${common}${content}`
    }

    /**
     * 文本消息
     * @param {*} data 
     */
    async text(data) {
        return `
            <Content><![CDATA[${data['content']}]]></Content>
        </xml>`
    }
    /**
     * 图片消息
     * @param {*} data 
     */
    async image(data) {
        return `
            <Image>
                <MediaId><![CDATA[${data['media_id']}]]></MediaId>
            </Image>
        </xml>`
    }

    /**
     * 语音消息
     * @param {*} data 
     */
    async voice(data) {
        return `
            <Voice>
                <MediaId><![CDATA[${data['media_id']}]]></MediaId>
            </Voice>
        </xml>`
    }

    /**
     * 视频消息
     * @param {*} data 
     */
    async video(data) {
        return `
            <Video>
                <MediaId><![CDATA[${data['media_id']}]]></MediaId>
                <Title><![CDATA[${data['title']}]]></Title>
                <Description><![CDATA[${data['description']}]]></Description>
            </Video>
        </xml>`
    }

    /**
     * 回复音乐消息
     * @param {*} data 
     */
    async music(data) {
        return `
            <Music>
                <Title><![CDATA[${data['title']}]]></Title>
                <Description><![CDATA[${data['description']}]]></Description>
                <MusicUrl><![CDATA[${data['music_url']}]]></MusicUrl>
                <HQMusicUrl><![CDATA[${data['hq_music_url']}]]></HQMusicUrl>
                <ThumbMediaId><![CDATA[${data['msgId']}]]></ThumbMediaId>
            </Music>
        </xml>`
    }

    /**
     * 回复图文消息
     * @param {*} data 
     */
    async news(data) {
        return `
            <ArticleCount>${data['articles'].length}</ArticleCount>
            ${data['articles']}
        </xml>`
    }
}

module.exports = new Xml()