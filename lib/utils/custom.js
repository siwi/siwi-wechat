/**
 * @author siwi
 * @email [siwi@siwi.me]
 * @create date 2018-03-13 11:32:34
 * @modify date 2018-03-13 11:32:34
 * @desc [客服消息]
 */
let instance = null

class Custom {
    constructor() {
        if (!instance) {
            instance = this
        }
        return instance
    }

    async createMessage(msgtype, data) {
        const common = {
            touser: data['openid'],
            msgtype: msgtype,
        }
        let content
        switch (msgtype) {
            case 'text':
                content = await this.text(data)
                break;
            case 'image':
                content = await this.image(data)
                break;
            case 'voice':
                content = await this.voice(data)
                break;
            case 'video':
                content = await this.video(data)
                break;
            case 'music':
                content = await this.music(data)
                break;
            case 'news':
                content = await this.news(data)
                break;
            case 'mpnews':
                content = await this.mpnews(data)
                break;
            case 'wxcard':
                content = await this.wxcard(data)
                break;
            default:
                break;
        }
        return Object.assign(common, content)
    }
    /**
     * 发送文字
     * @param {*} data 
     */
    async text(data) {
        return {
            text: {
                content: data['content']
            }
        }
    }
    /**
     * 发送图片
     * @param {*} data 
     */
    async image(data) {
        return {
            image: {
                media_id: data['media_id']
            }
        }
    }
    /**
     * 发生语音消息
     * @param {*} data 
     */
    async voice(data) {
        return {
            voice: {
                media_id: data['media_id']
            }
        }
    }
    /**
     * 发送视频消息
     * @param {*} data 
     */
    async video(data) {
        return {
            video: {
                media_id: data['media_id'],
                thumb_media_id: data['thumb_media_id'],
                title: data['title'],
                description: data['description'],
            }
        }
    }
    /**
     * 发送音乐消息
     * @param {*} data 
     */
    async music(data) {
        return {
            music: {
                title: data['title'],
                description: data['description'],
                musicurl: data['musicurl'],
                hqmusicurl: data['hqmusicurl'],
                thumb_media_id: data['thumb_media_id']
            }
        }
    }
    /**
     * 发送图文消息（点击跳转到外链） 图文消息条数限制在8条以内，注意，如果图文数超过8，则将会无响应。
     * @param {*} data 
     */
    async news(data) {
        return {
            news: {
                articles: data['articles']
            }
        }
    }
    /**
     * 发送图文消息（点击跳转到图文消息页面） 图文消息条数限制在8条以内，注意，如果图文数超过8，则将会无响应。
     * @param {*} data 
     */
    async mpnews(data) {
        return {
            mpnews: {
                media_id: data['media_id']
            }
        }
    }
    /**
     * 发送卡券
     * @param {*} data 
     */
    async wxcard(data) {
        return {
            wxcard: {
                card_id: data['card_id']
            },
        }
    }
}

module.exports = new Custom()