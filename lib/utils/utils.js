let instance = null
const xml2js = require('xml2js')
class Utils {
    constructor() {
        if (!instance) {
            instance = this
        }
        return instance
    }
    /**
     * check error 
     * @param {*} data 
     */
    async checkError (data) {
        if (data['errcode']) {
            return false
        }
        return true
    }

    /**
     * xml to json
     * @param {*} xml 
     */
    async xmlToJson (xml) {
        return new Promise((resolve, reject) => {
            const xmlParse = xml2js.parseString
            xmlParse(xml, {explicitArray: false}, (err, result) => {
                if (err) {
                    reject(err)
                }
                resolve(result)
            })
        })
    }
    /**
     * json to xml
     * @param {*} json 
     */
    async jsonToXml (json) {
        const builder = new  xml2js.Builder()
        return builder.buildObject(json)
    }

    /**
     * 统一输出
     * @param {Object} res 
     */
    async output (res) {
        
    }
}

module.exports = Utils