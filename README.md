

[![node](https://img.shields.io/node/v/siwi-wechat.svg)](https://www.npmjs.com/package/siwi-wechat)
[![npm](https://img.shields.io/npm/v/siwi-wechat.svg)](https://www.npmjs.com/package/siwi-wechat)
[![npm](https://img.shields.io/npm/dt/siwi-wechat.svg)](https://www.npmjs.com/package/siwi-wechat)

# siwi-wechat
wechat api 封装

# lib/mixin 

* Wechat
* User
* Menu
* Message

# lib/module

* cache
* xml
* message

> 一些功能模块 缓存等


* options

```js

const options = {
    account: 'test',
    appid: 'wxa9287ae8ca583278',
    appsecret: 'b5cc39d030d739a94a24ed537256b2ad',
    token: 'wxz',
    redis_options: {
        host: '192.168.10.10'
    },
    cache: {
        drive: 'file',
        file: {
            path: `${process.env.PWD}/cache`
        },
        redis:{
            host: '192.168.10.10'
        },
    }
}
```

* 文件缓存 or redis缓存


# 单元测试
- 获取永久素材
- 获取临时素材

# mix模式 

# 文档

# 测试
## 通过的列表

## 未通过的列表

## 关于支付