const options = require('./config')
const Wechat = require('../lib/mixin/wechat')
const User = require('../lib/mixin/user')
const expect = require('chai').expect
const Mixin = require('siwi-mixin')
describe('mixin.js', () => {
    describe('#mixin', function () {
        it('测试mixin', async function () {
            const WechatMixin = await Mixin.mix(Wechat, User)
            const wechat = new WechatMixin(options)
            const openid = 'ozP--s3_IZfUCZI_NuLXs0KTuR08'
            const user = await wechat.userInfo(openid)
            console.log(user)
        })
    })
})