const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect
const path = require('path')
describe('resource.js', function () {
    describe('#mediaUpload', function () {
        it('上传临时素材 图片', async function () {
            const file = path.join(__dirname, 'data', 'jenny.jpg')
            const res = await wechat.mediaUpload(file, 'image')
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('media_id')
        })
    })
    // 获取临时素材 TODO
    // describe('#mediaGet', function () {
    //     it('获取临时素材', async function () {
    //         const res = await wechat.mediaGet('xYIdoQdaqqpDGkIkQiBbFuwa_LeR8onCE98rExhTg5iZgt9pzW2Fv0GHKmOOB2vs')
    //         console.log(res)
    //         expect(res).to.be.an('String')
    //         // expect(res).to.have.property('media_id')
    //     })
    // })
    describe('#mediaUploadImg', function () {
        it('上传图文消息内的图片获取URL', async function () {
            const file = path.join(__dirname, 'data', 'jenny.jpg')
            const res = await wechat.mediaUploadImg(file)
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('url')
        })
    })
    describe('#materialAddMaterial', function () {
        it('新增永久素材', async function () {
            const file = path.join(__dirname, 'data', 'jenny.jpg')
            const res = await wechat.materialAddMaterial(file, 'image')
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('media_id')
        })
    })
    describe('#materialDelMaterial', function () {
        it('新增永久素材', async function () {
            const file = path.join(__dirname, 'data', 'jenny.jpg')
            const media = await wechat.materialAddMaterial(file, 'image')
            const media_id = media['media_id']
            const res = await wechat.materialDelMaterial(media_id)
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })

    // 获取临时素材 TODO
    // describe('#materialGetMaterial', function () {
    //     it('获取永久素材', async function () {

    //     })
    // })

    describe('#materialAddNews', function () {
        it('新增永久图文素材', async function () {
            const articles = [{
                title: '图文测试',
                thumb_media_id: 'fS2dJtM25dKQbNmTGRbYoIEDVadVE3dzE6RkcP4LqrU',
                author: 'Mankong',
                digest: '这是很好的测试',
                show_cover_pic: 1,
                content: '你好啊, 这是很好的测试 多谢你了',
                content_source_url: 'https://siwi.me'
            }]
            const res = await wechat.materialAddNews(articles)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('media_id')
        })
    })

    describe('#materialUpdateNews', function () {
        it('新增永久图文素材', async function () {
            const meidia_id = 'fS2dJtM25dKQbNmTGRbYoJTm_VKhWgDwmtFgNsRySUY'
            const index = 0;
            const articles = {
                title: '图文测试修改',
                thumb_media_id: 'fS2dJtM25dKQbNmTGRbYoIEDVadVE3dzE6RkcP4LqrU',
                author: 'Mankong',
                digest: '这是很好的测试修改 你好啊',
                show_cover_pic: 1,
                content: '你好啊, 这是很好的测试 多谢你了',
                content_source_url: 'https://siwi.me'
            }
            const res = await wechat.materialUpdateNews(meidia_id, index, articles)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })

    describe('#materialGetMaterialCount', function () {
        it('获取素材总数', async function () {
            const res = await wechat.materialGetMaterialCount()
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('voice_count')
            expect(res).to.have.property('video_count')
            expect(res).to.have.property('image_count')
            expect(res).to.have.property('news_count')
        })
    })
    describe('#materialBatchGetMaterial', function () {
        it('获取素材列表', async function () {
            const res = await wechat.materialBatchGetMaterial('image', 0, 20)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('total_count')
        })
    })
})