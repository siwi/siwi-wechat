const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('menu.js', function () {
    describe('#menuDelete', function () {
        it('自定义菜单删除接口', async function () {
            const res = await wechat.menuDelete()
            expect(res).to.be.an('Object')
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })
    describe('#menuCreate', function () {
        it('自定义菜单创建接口', async function () {
            const button = [{
                    "name": "扫码",
                    "sub_button": [{
                            "type": "scancode_waitmsg",
                            "name": "扫码带提示",
                            "key": "rselfmenu_0_0",
                            "sub_button": []
                        },
                        {
                            "type": "scancode_push",
                            "name": "扫码推事件",
                            "key": "rselfmenu_0_1",
                            "sub_button": []
                        }
                    ]
                },
                {
                    "name": "发图",
                    "sub_button": [{
                            "type": "pic_sysphoto",
                            "name": "系统拍照发图",
                            "key": "rselfmenu_1_0",
                            "sub_button": []
                        },
                        {
                            "type": "pic_photo_or_album",
                            "name": "拍照或者相册发图",
                            "key": "rselfmenu_1_1",
                            "sub_button": []
                        },
                        {
                            "type": "pic_weixin",
                            "name": "微信相册发图",
                            "key": "rselfmenu_1_2",
                            "sub_button": []
                        }
                    ]
                },
                {
                    "name": "发送位置",
                    "type": "location_select",
                    "key": "rselfmenu_2_0"
                }
            ]

            const res = await wechat.menuCreate(button)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })
    describe('#menuGet', function () {
        it('自定义菜单查询接口', async function () {
            const res = await wechat.menuGet()
            expect(res).to.be.an('Object')
            expect(res).to.have.property('menu')
        })
    })
    describe('#getCurrentSelfMenuInfo', function () {
        it('获取自定义菜单配置接口', async function () {
            const res = await wechat.getCurrentSelfMenuInfo()
            expect(res).to.be.an('Object')
            expect(res).to.have.property('is_menu_open')
            expect(res.is_menu_open).to.equal(1)
        })
    })
    describe('#menuAddConditional', function () {
        it('获取自定义菜单配置接口', async function () {
            const button = [{
                    "type": "click",
                    "name": "测试创建",
                    "key": "V1001_TODAY_MUSIC"
                },
                {
                    "name": "菜单",
                    "sub_button": [{
                            "type": "view",
                            "name": "搜索",
                            "url": "http://www.soso.com/"
                        },
                        {
                            "type": "click",
                            "name": "赞一下我们",
                            "key": "V1001_GOOD"
                        }
                    ]
                }
            ]
            const matchrule = {
                "tag_id": "0",
                "sex": "1",
                "country": "中国",
                "province": "广东",
                "city": "深圳",
                "client_platform_type": "2",
                "language": "zh_CN"
            }
            const res = await wechat.menuAddConditional(button, matchrule)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('menuid')
            const menuid = res['menuid']
            await wechat.menuDelConditional(menuid)
        })
    })
    describe('#menuDelConditional', function () {
        let button, matchrule
        beforeEach(function () {
            button = [{
                    "type": "click",
                    "name": "测试删除",
                    "key": "V1001_TODAY_MUSIC"
                },
                {
                    "name": "个性化菜单",
                    "sub_button": [{
                            "type": "view",
                            "name": "搜索",
                            "url": "http://www.soso.com/"
                        },
                        {
                            "type": "click",
                            "name": "赞一下我们",
                            "key": "V1001_GOOD"
                        }
                    ]
                }
            ]
            matchrule = {
                "tag_id": "0",
                "sex": "1",
                "country": "中国",
                "province": "广东",
                "city": "深圳",
                "client_platform_type": "2",
                "language": "zh_CN"
            }
        })
        it('删除个性化菜单', async function () {
            const menu = await wechat.menuAddConditional(button, matchrule)
            const menuid = menu['menuid']
            const res = await wechat.menuDelConditional(menuid)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })
    describe('#menuTryMatch', function () {
        let button, matchrule
        beforeEach(function () {
            button = [{
                    "type": "click",
                    "name": "测试匹配",
                    "key": "V1001_TODAY_MUSIC"
                },
                {
                    "name": "个性化菜单",
                    "sub_button": [{
                            "type": "view",
                            "name": "搜索",
                            "url": "http://www.soso.com/"
                        },
                        {
                            "type": "click",
                            "name": "赞一下我们",
                            "key": "V1001_GOOD"
                        }
                    ]
                }
            ]
            matchrule = {
                "tag_id": "0",
                "sex": "1",
                "country": "中国",
                "province": "广东",
                "city": "深圳",
                "client_platform_type": "2",
                "language": "zh_CN"
            }
        })
        it('测试个性化菜单匹配结果', async function () {
            const menu = await wechat.menuAddConditional(button, matchrule)
            const menuid = menu['menuid']
            const user_id = 'ozP--s3_IZfUCZI_NuLXs0KTuR08'
            const res = await wechat.menuTryMatch(user_id)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('menu')
            await wechat.menuDelConditional(menuid)
        })
    })
})