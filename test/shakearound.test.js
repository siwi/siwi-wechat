const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('shakearound.js', () => {
    describe('#shakeAroundAccountRegister', function () {
        it('获取素材列表', async function () {
            const res = await wechat.shakeAroundAccountRegister()
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundAccountAuditStatus', () => {
        it('查询审核状态', async () => {
            const res = await wechat.shakeAroundAccountAuditStatus()
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundDeviceApplyId', () => {
        it('', async () => {
            const quantity = 3
            const apply_reason = '测试'
            const comment = '测试专用'
            const poi_id = '1234'
            const res = await wechat.shakeAroundDeviceApplyId(quantity, apply_reason, comment, poi_id)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundDeviceApplyStatus', () => {
        it('', async () => {
            const apply_id = 3
            const res = await wechat.shakeAroundDeviceApplyStatus(apply_id)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundDeviceUpdate', () => {
        it('', async () => {
            const device_id = 10011
            const uuid = 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825'
            const major = 1002
            const minor = 1223
            const comment = 'test'
            const res = await wechat.shakeAroundDeviceUpdate(device_id, uuid, major, minor, comment)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundDeviceBindLocation', () => {
        it('', async () => {
            const device_id = 10011
            const uuid = 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825'
            const major = 1002
            const minor = 1223
            const poi_id = 1234
            const res = await wechat.shakeAroundDeviceUpdate(device_id, uuid, major, minor, poi_id)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundDeviceBindOtherLocation', () => {
        it('', async () => {
            const device_id = 10011
            const uuid = 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825'
            const major = 1002
            const minor = 1223
            const poi_id = 1234
            const type = 2
            const poi_appid = 'wxappid'
            const res = await wechat.shakeAroundDeviceBindOtherLocation(device_id, uuid, major, minor, poi_id, type, poi_appid)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundDeviceSearch', () => {
        it('', async () => {
            const device_id = 10011
            const uuid = 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825'
            const major = 1002
            const minor = 1223
            const poi_id = 1234
            const type = 2
            const poi_appid = 'wxappid'
            const last_seen = 1
            const count = 100
            const res = await wechat.shakeAroundDeviceSearch(device_id, uuid, major, minor, poi_id, type, poi_appid, last_seen, count)
            console.log(res)
        })
    })


    describe('#shakeAroundPageAdd', () => {
        it('', async () => {
            const title = "主标题"
            const description = "副标题"
            const page_url = " https://zb.weixin.qq.com "
            const comment = "数据示例"
            const icon_url = "http://3gimg.qq.com/shake_nearby/dy/icon "

            const res = await wechat.shakeAroundPageAdd(title, description, icon_url, page_url, comment)
            console.log(res)
        })
    })
    describe('#shakeAroundPageUpdate', () => {
        it('', async () => {
            const page_id = 1
            const title = "主标题"
            const description = "副标题"
            const page_url = " https://zb.weixin.qq.com "
            const comment = "数据示例"
            const icon_url = "http://3gimg.qq.com/shake_nearby/dy/icon "

            const res = await wechat.shakeAroundPageUpdate(page_id, title, description, icon_url, page_url, comment)
            console.log(res)
        })
    })
    describe('#shakeAroundPageSearch', () => {
        it('', async () => {
            const type = 1
            const page_ids = [1, 2, 3, 4, 5, 6, 7]

            const res = await wechat.shakeAroundPageSearch(type, page_ids, false, false)
            console.log(res)
        })
        it('', async () => {
            const type = 2
            const begin = 2
            const count = 3
            const res = await wechat.shakeAroundPageSearch(type, false, begin, count)
            console.log(res)
        })
    })
    describe('#shakeAroundPageDelete', () => {
        it('', async () => {
            const page_id = 10
            const res = await wechat.shakeAroundPageDelete(page_id)
            console.log(res)
        })
    })
    describe('#shakeAroundMaterialAdd', () => {
        it('', async () => {
            const file = path.join(__dirname, 'data', 'jenny.jpg')
            const res = await wechat.shakeAroundMaterialAdd(file)
            console.log(res)
        })
    })

    describe('#shakeAroundDeviceBindPage', () => {
        it('', async () => {
            const device_id = 10011
            const uuid = 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825'
            const major = 1002
            const minor = 1223
            const page_ids = [12345, 23456, 334567]
            const res = await wechat.shakeAroundDeviceBindPage(device_id, uuid, major, minor, page_ids)
            console.log(res)
        })
    })
    describe('#shakeAroundRelationSearch', () => {
        it('', async () => {
            const type = 1
            const device_id = 10011
            const uuid = 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825'
            const major = 1002
            const minor = 1223
            const begin = 2
            const count = 3
            const page_ids = [12345, 23456, 334567]
            const res = await wechat.shakeAroundRelationSearch(type, device_id, uuid, major, minor, page_id, begin, count)
            console.log(res)
        })
        it('', async () => {
            const type = 1
            const device_id = 10011
            const uuid = 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825'
            const major = 1002
            const minor = 1223
            const begin = 2
            const count = 3
            const page_ids = [12345, 23456, 334567]
            const res = await wechat.shakeAroundRelationSearch(type, device_id, uuid, major, minor, page_id, begin, count)
            console.log(res)
        })
    })
    describe('#shakeAroundStatisticsDevice', () => {
        it('', async () => {
            const type = 1
            const device_id = 10011
            const uuid = 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825'
            const major = 1002
            const minor = 1223
            const begin_date = 1438704000
            const end_date = 1438704000
            const res = await wechat.shakeAroundStatisticsDevice(device_id, uuid, major, minor, begin_date, end_date)
            console.log(res)
        })
    })
    describe('#shakeAroundStatisticsDeviceList', () => {
        it('', async () => {
            const date = 1438704000
            const page_index = 1
            const res = await wechat.shakeAroundStatisticsDeviceList(date, page_index)
            console.log(res)
        })
    })
    describe('#shakeAroundStatisticsPage', () => {
        it('', async () => {
            const page_id = 12345
            const begin_date = 1438704000
            const end_date = 1438704000
            const res = await wechat.shakeAroundStatisticsPage(page_id, begin_date, end_date)
            console.log(res)
        })
    })
    describe('#shakeAroundStatisticsPageList', () => {
        it('', async () => {
            const page_index = 12345
            const date = 1438704000
            const res = await wechat.shakeAroundStatisticsPageList(date, page_index)
            console.log(res)
        })
    })

    describe('#shakeAroundDeviceGroupAdd', () => {
        it('', async () => {
            const group_name = 'test'
            const res = await wechat.shakeAroundDeviceGroupAdd(group_name)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundDeviceGroupUpdate', () => {
        it('', async () => {
            const group_id = 123
            const group_name = 'test update'
            const res = await wechat.shakeAroundDeviceGroupUpdate(group_id, group_name)
            console.log(res)
            expect(res).to.be.an('object')

        })
    })
    describe('#shakeAroundDeviceGroupDelete', () => {
        it('', async () => {
            const group_id = 123
            const res = await wechat.shakeAroundDeviceGroupDelete(group_id)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundDeviceGroupGetList', () => {
        it('', async () => {
            const begin = 0
            const count = 10
            const res = await wechat.shakeAroundDeviceGroupGetList(begin, count)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundDeviceGroupGetDetail', () => {
        it('', async () => {
            const begin = 0
            const count = 10
            const group_id = 1
            const res = await wechat.shakeAroundDeviceGroupGetDetail(group_id, begin, count)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundDeviceGroupAddDevice', () => {
        it('', async () => {
            const group_id = 1
            const device_id = 10011
            const uuid = 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825'
            const major = 1002
            const minor = 1223
            const res = await wechat.shakeAroundDeviceGroupAddDevice(group_id, device_id, uuid, major, minor)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundDeviceGroupDeleteDevice', () => {
        it('', async () => {
            const group_id = 1
            const device_id = 10011
            const uuid = 'FDA50693-A4E2-4FB1-AFCF-C6EB07647825'
            const major = 1002
            const minor = 1223
            const res = await wechat.shakeAroundDeviceGroupDeleteDevice(group_id, device_id, uuid, major, minor)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#shakeAroundUserGetShakeInfo', () => {
        it('', async () => {
            const ticket = '6ab3d8465166598a5f4e8c1b44f44645'
            const need_poi = 1
            const res = await wechat.shakeAroundUserGetShakeInfo(ticket, need_poi)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#mmpaymkTtransfersBbPreOrder', () => {
        it('', async () => {

        })
    })
    describe('#shakeAroundLotteryAddLotteryInfo', () => {
        it('', async () => {
            const use_template = 1
            const logo_url = 'test.jpg'
            const title = "title"
            const desc = "desc"
            const onoff = 1
            const begin_time = 1428854400
            const expire_time = 1428940800
            const sponsor_appid = "wxxxxxxxxxxxxxx"
            const total = 10
            const jump_url = JUMP_URL
            const key = "keyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"
            const res = await wechat.shakeAroundLotteryAddLotteryInfo(use_template, logo_url, title, desc, onoff, begin_time, expire_time, sponsor_appid, total, jump_url, key)
            console.log(res)
        })
    })
    describe('#shakeAroundLotterySetPrizeBucket', () => {
        it('', async () => {
            const lottery_id = 'xxxxxxllllll'
            const mchid = '10000098'
            const sponsor_appid = 'wx8888888888888888'
            const prize_info_list = [{
                    "ticket": "v1|ZiPs2l0hpMBp3uwGI1rwp45vOdz/V/zQ/00jP9MeWT+e47/q1FJjwCIP34frSjzOxAEzJ7k2CtAg1pmcShvkChBWqbThxPm6MBuzceoHtj79iHuHaEn0WAO+j4sXnXnbGswFOlDYWg1ngvrRYnCY3g=="
                },
                {
                    "ticket": "v1|fOhNUTap1oepSm5ap0hx1gmATM\/QX\/xn3sZWL7K+5Z10sbV5\/mZ4SwxwxbK2SPV32eLRvjd4ww1G3H5a+ypqRrySi+4oo97y63KoEQbRCPjbkyQBY8AYVyvD40V2b9slTQCm2igGY98mPe+VxZiayQ=="
                }
            ]
            const res = await wechat.shakeAroundLotterySetPrizeBucket(lottery_id, mchid, sponsor_appid, prize_info_list)

        })
    })
    describe('#shakeAroundLotterySetLotterySwitch', () => {
        it('', async () => {
            const lottery_id = 1
            const onoff = 1
            const res = await wechat.shakeAroundLotterySetLotterySwitch(lottery_id, onoff)
            console.log(res)
        })
    })
    describe('#shakeAroundLotteryQueryLottery', () => {
        it('', async () => {
            const lottery_id = 1
            const res = await wechat.shakeAroundLotteryQueryLottery(lottery_id)
            console.log(res)
        })
    })
})