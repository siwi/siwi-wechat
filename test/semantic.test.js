const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('datacube.js', () => {
    // describe('#semanticSemproxySearch', () => {
    //     it('语义理解', async () => {
    //         const msg_data_id = 1
    //         const index = 0

    //         const query = '查一下明天从北京到上海的南航机票'
    //         const city = '北京'
    //         const category = 'flight,hotel'
    //         const appid = 'wxa9287ae8ca583278'
    //         const uid = 'ozP--s3_IZfUCZI_NuLXs0KTuR08'
    //         const res = await wechat.semanticSemproxySearch(query, category, appid, uid, city)
    //         console.log(JSON.stringify(res))
    //         expect(res).to.be.an('object')
    //     })
    // })
    describe('#mediaVoiceTranslateContent', () => {
        it('微信翻译', async () => {
            const lfrom = 'zh_CN'
            const lto = 'en_US'
            const body = '查一下明天从北京到上海的南航机票'
            const res = await wechat.mediaVoiceTranslateContent(body, lfrom, lto)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })

})