const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('poi.js', () => {
    // describe('#poiAddPoi', function () {
    //     it('获取素材列表', async function () {
    //         const data = {
    //             "business": {
    //                 "base_info": {
    //                     "sid": "33788392",
    //                     "business_name": "南哲时尚",
    //                     "branch_name": "南山分店",
    //                     "province": "广东省",
    //                     "city": "深圳市",
    //                     "district": "南山区",
    //                     "address": "科园一路科兴科学院",
    //                     "telephone": "0755-32823003",
    //                     "categories": ["美食,小吃快餐"],
    //                     "offset_type": 1,
    //                     "longitude": 115.32375,
    //                     "latitude": 25.097486,
    //                     "photo_list": [{
    //                         "photo_url": "http://mmbiz.qpic.cn/mmbiz_jpg/ySOl0w1V5Wls2FWRa1LjrJ8ssG5HQJqniaHqasL2nsLVwqYtcSZ3SJcPwCiadShJqYCbmWsIS2wBt8n5Az9s6QdQ/0?wx_fmt=jpeg"
    //                     }],
    //                     "recommend": "欧美时尚潮流前线",
    //                     "special": "免费wifi",
    //                     "introduction": "创立元2016年 最大的时尚公司",
    //                     "open_time": "8:00-20:00",
    //                     "avg_price": 350
    //                 }
    //             }
    //         }
    //         const res = await wechat.poiAddPoi(data)
    //         console.log(res)
    //         expect(res).to.be.an('object')
    //     })
    // })

    // describe('#poiGetPoi', () => {
    //     it('查询门店信息', async () => {
    //         const poi_id = '271262077'
    //         const res = await wechat.poiGetPoi(poi_id)
    //         console.log(res)
    //     })
    // })
    // describe('#poiGetPoiList', () => {
    //     it('查询门店列表', async () => {
    //         const begin = 0
    //         const limit = 0
    //         const res = await wechat.poiGetPoiList(begin, limit)
    //         console.log(res)
    //     })
    // })
    // describe('#poiUpdatePoi', () => {
    //     it('修改门店服务信息', async () => {
    //         const data = {
    //             "business ": {
    //                 "base_info": {
    //                     "poi_id ": "271864249",
    //                     "sid": "A00001",
    //                     "telephone ": "020-12345678",
    //                     "photo_list": [{
    //                         "photo_url": "https:// XXX.com"
    //                     }, {
    //                         "photo_url": "https://XXX.com"
    //                     }],
    //                     "recommend": "麦辣鸡腿堡套餐，麦乐鸡，全家桶",
    //                     "special": "免费wifi，外卖服务",
    //                     "introduction": "麦当劳是全球大型跨国连锁餐厅，1940 年创立于美国，在世界上大约拥有3 万间分店。主要售卖汉堡包，以及薯条、炸鸡、汽水、冰品、沙拉、水果等快餐食品",
    //                     "open_time": "8:00-20:00",
    //                     "avg_price": 35
    //                 }
    //             }
    //         }
    //         const res = await wechat.poiUpdatePoi(data)
    //         console.log(res)
    //     })
    // })
    // describe('#poiDelPoi', () => {
    //     it('删除门店', async () => {
    //         const poi_id = 1024
    //         const res = await wechat.poiDelPoi(poi_id)
    //         console.log(res)
    //     })
    // })
    // describe('#poiGetWxCategory', () => {
    //     it('门店类目表', async () => {
    //         const res = await wechat.poiGetWxCategory()
    //         console.log(res)
    //     })
    // })
    // describe('#wxaGetMerchantCategory', () => {
    //     it('小程序门店类目表', async () => {
    //         const res = await wechat.wxaGetMerchantCategory()
    //         console.log(res)
    //     })
    // })
    // describe('#wxaApplyMerchant', () => {
    //     it('小程序门店类目表', async () => {
    //         const data = {
    //             "first_catid": 476, 
    //             "second_catid": 477,
    //             "qualification_list": "RTZgKZ386yFn5kQSWLTxe4bqxwgzGBjs3OE02cg9CVQk1wRVE3c8fjUFX7jvpi-P",
    //             "headimg_mediaid": "RTZgKZ386yFn5kQSWLTxe4bqxwgzGBjs3OE02cg9CVQk1wRVE3c8fjUFX7jvpi-P",
    //             "nickname": "hardenzhang308",
    //             "intro": "hardenzhangtest",
    //             "org_code": "",
    //             "other_files": ""
    //         }
    //         const res = await wechat.wxaApplyMerchant(data)
    //         console.log(res)
    //     })
    // })

    // describe('#wxaGetMerchantAuditInfo', () => {
    //     it('查询门店小程序审核结果',async () => {
    //         const res = await wechat.wxaGetMerchantAuditInfo()
    //         console.log(res)
    //     })
    // })
    // describe('#wxaModifyMerchant', () => {
    //     it('修改门店小程序信息',async () => {
    //         const headimg_mediaid = 'headimg_mediaid'
    //         const intro = ''
    //         const res = await wechat.wxaModifyMerchant(headimg_mediaid, intro)
    //         console.log(res)
    //     })
    // })

    // describe('#wxaGetDistrict', () => {
    //     it('从腾讯地图拉取省市区信息', async () => {
    //         const res = await wechat.wxaGetDistrict(headimg_mediaid, intro)
    //         console.log(res)
    //     })
    // })
    // describe('#wxaSearchMapPoi', () => {
    //     it('在腾讯地图中搜索门店', async () => {
    //         const districtid = '440105'
    //         const keyword = "x"
    //         const res = await wechat.wxaSearchMapPoi(districtid, keyword)
    //         console.log(res)
    //     })
    // })

    // describe('#wxaCreateMapPoi', () => {
    //     it('在腾讯地图中创建门店', async () => {
    //         const data = {
    //             "name": "hardenzhang",
    //             "longitude": "113.323753357",
    //             "latitude": "23.0974903107",
    //             "province": "广东省",
    //             "city": "广州市",
    //             "district": "海珠区",
    //             "address": "TIT",
    //             "category": "类目1:类目2",
    //             "telephone": "12345678901",
    //             "photo": "http://mmbiz.qpic.cn/mmbiz_png/tW66AWE2K6ECFPcyAcIZTG8RlcR0sAqBibOm8gao5xOoLfIic9ZJ6MADAktGPxZI7MZLcadZUT36b14NJ2cHRHA/0?wx_fmt=png",
    //             "license": "http://mmbiz.qpic.cn/mmbiz_png/tW66AWE2K6ECFPcyAcIZTG8RlcR0sAqBibOm8gao5xOoLfIic9ZJ6MADAktGPxZI7MZLcadZUT36b14NJ2cHRHA/0?wx_fmt=png",
    //             "introduct": "test",
    //             "districtid": "440105",
    //         }
    //         const res = await wechat.wxaCreateMapPoi(data)
    //         console.log(res)
    //     })
    // })
    // describe('#wxaAddStore', () => {
    //     it('添加门店', async () => {
    //         const data = {
    //             "poi_id": "",
    //             "map_poi_id": "2880741500279549033",
    //             "pic_list": "{\"list\":[\"http://mmbiz.qpic.cn/mmbiz_jpg/tW66AWvE2K4EJxIYOVpiaGOkfg0iayibiaP2xHOChvbmKQD5uh8ymibbEKlTTPmjTdQ8ia43sULLeG1pT2psOfPic4kTw/0?wx_fmt=jpeg\"]}",
    //             "contract_phone": "1111222222",
    //             "credential": "22883878-0",
    //             "qualification_list": "RTZgKZ386yFn5kQSWLTxe4bqxwgzGBjs3OE02cg9CVQk1wRVE3c8fjUFX7jvpi-P"
    //         }
    //         const res = await wechat.wxaAddStore(data)
    //         console.log(res)
    //     })
    // })
    // describe('#wxaUpdateStore', () => {
    //     it('更新门店信息', async () => {
    //         const data = {
    //             "map_poi_id": "5938314494307741153",
    //             "poi_id": "472671857",
    //             "hour": "10:00-21:00",
    //             "contract_phone": "123456",
    //             "pic_list": "{\"list\":[\"http://mmbiz.qpic.cn/mmbiz_jpg/tW66AWvE2K4EJxIYOVpiaGOkfg0iayibiaP2xHOChvbmKQD5uh8ymibbEKlTTPmjTdQ8ia43sULLeG1pT2psOfPic4kTw/0?wx_fmt=jpeg\"]}"
    //         }
    //         const res = await wechat.wxaUpdateStore(data)
    //         console.log(res)
    //     })
    // })
    // describe('#wxaGetStoreInfo', () => {
    //     it('获取单个门店信息', async () => {
    //         const poi_id = 472671857
    //         const res = await wechat.wxaGetStoreInfo(poi_id)
    //         console.log(res)
    //     })
    // })
    // describe('#wxaGetStoreList', () => {
    //     it('获取门店信息列表', async () => {
    //         const offset = 0
    //         const limit = 10
    //         const res = await wechat.wxaGetStoreList(offset, limit)
    //         console.log(res)
    //     })
    // })
    // describe('#wxaDelStore', () => {
    //     it('删除门店', async () => {
    //         const poi_id = 472671857
    //         const res = await wechat.wxaDelStore(poi_id)
    //         console.log(res)
    //     })
    // })
})