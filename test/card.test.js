const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('card.js', () => {
    // describe('#materialBatchGetMaterial', function () {
    //     it('获取素材列表', async function () {
    //         const res = await wechat.materialBatchGetMaterial('image', 0, 20)
    //         console.log(res)
    //         expect(res).to.be.an('object')
    //         expect(res).to.have.property('total_count')
    //     })
    // })
    describe('#cardCreate', () => {
        it('创建卡券', async () => {
            const data = {
                "card": {
                    "card_type": "GROUPON",
                    "groupon": {
                        "base_info": {
                            "logo_url": "http://mmbiz.qpic.cn/mmbiz_jpg/ySOl0w1V5Wls2FWRa1LjrJ8ssG5HQJqniaHqasL2nsLVwqYtcSZ3SJcPwCiadShJqYCbmWsIS2wBt8n5Az9s6QdQ/0?wx_fmt=jpeg",
                            "brand_name": "微信餐厅",
                            "code_type": "CODE_TYPE_TEXT",
                            "title": "132元双人火锅套餐",
                            "sub_title": "周末狂欢必备",
                            "color": "Color010",
                            "notice": "使用时向服务员出示此券",
                            "service_phone": "020-88888888",
                            "description": "不可与其他优惠同享\n如需团购券发票，请在消费时向商户提出\n店内均可使用，仅限堂食",
                            "date_info": {
                                "type": "DATE_TYPE_FIX_TERM",
                                "fixed_term": 15,
                                "fixed_begin_term": 0,
                                "begin_timestamp": Math.floor(Date.now()/ 1000)- 86400 * 1,
                                "end_timestamp": Math.floor(Date.now()/ 1000) + 86400 *10
                            },
                            "sku": {
                                "quantity": 500000
                            },
                            "get_limit": 3,
                            "use_custom_code": false,
                            "bind_openid": false,
                            "can_share": true,
                            "can_give_friend": true,
                            "location_id_list": [123, 12321, 345345],
                            "custom_url_name": "立即使用",
                            "custom_url": "http://www.qq.com",
                            "custom_url_sub_title": "6个汉字tips",
                            "promotion_url_name": "更多优惠",
                            "promotion_url": "http://www.qq.com"
                        },
                        "deal_detail": "以下锅底2选1（有菌王锅、麻辣锅、大骨锅、番茄锅、清补凉锅、酸 菜鱼锅可选）：\n大锅1份 12元\n小锅2份 16元 "
                    }

                }
            }
            const res = await wechat.cardCreate(data)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
})