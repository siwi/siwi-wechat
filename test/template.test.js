const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('template.js', () => {
    describe('#templateApiSetIndustry', () => {
        it('设置所属行业', async () => {
            const industry_id1 = 1
            const industry_id2 = 4
            const res = await wechat.templateApiSetIndustry(industry_id1, industry_id2)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })
    describe('#templateGetIndustry', () => {
        it('获取设置的行业信息', async () => {
            const res = await wechat.templateGetIndustry()
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('primary_industry')
            expect(res).to.have.property('secondary_industry')
        })
    })
    describe('#templateApiAddTemplate', () => {
        it('获得模板ID', async () => {
            const template_id_short = 'TM00001'
            const res = await wechat.templateApiAddTemplate(template_id_short)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('template_id')
        })
    })

    describe('#templateGetAllPrivateTemplate', () => {
        it('获取模板列表', async () => {
            const res = await wechat.templateGetAllPrivateTemplate()
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('template_list')
        })
    })
    describe('#templateDelPrivateTemplate', () => {
        it('删除模板', async () => {
            const template_id_short = 'TM00002'
            const template = await wechat.templateApiAddTemplate(template_id_short)
            const res = await wechat.templateDelPrivateTemplate(template['template_id'])
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })
    describe('#templateSend', () => {
        it('发送模板消息', async () => {
            const data = {
                "touser":"ozP--s3_IZfUCZI_NuLXs0KTuR08",
                "template_id":"QGiAH-8ZtspIo6rkyjPEhvc4kXIbN4UtXMWxR7uwtR4",
                "url":"https://xia.sznzkj.cn",           
                "data":{
                        "name":{
                            "value":"超级大保健",
                            "color":"#173177"
                        },
                        "remark":{
                            "value":"欢迎再次购买！",
                            "color":"#173177"
                        }
                }
            }
            const res = await wechat.templateSend(data)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })

    })
})