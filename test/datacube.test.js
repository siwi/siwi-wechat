const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('datacube.js', () => {
    describe('#dataCubeGetUserSummary', () => {
        it('获取用户增减数据', async () => {
            const begin_date = '2018-03-23'
            const end_date = '2018-03-23'
            const res = await wechat.dataCubeGetUserSummary(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataCubeGetUserCumulate', () => {
        it('获取累计用户数据', async () => {
            const begin_date = '2018-03-20'
            const end_date = '2018-03-23'
            const res = await wechat.dataCubeGetUserCumulate(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetArticleSummary', () => {
        it('获取图文群发每日数据', async () => {
            const begin_date = '2018-03-23'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetArticleSummary(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetArticleTotal', () => {
        it('获取图文群发总数据', async () => {
            const begin_date = '2018-03-23'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetArticleTotal(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetUserRead', () => {
        it('获取图文统计数据', async () => {
            const begin_date = '2018-03-21'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetUserRead(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetUserReadHour', () => {
        it('获取图文统计分时数据', async () => {
            const begin_date = '2018-03-23'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetUserReadHour(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetUserShare', () => {
        it('获取图文分享转发数据', async () => {
            const begin_date = '2018-03-20'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetUserShare(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetUserShareHour', () => {
        it('获取图文分享转发分时数据', async () => {
            const begin_date = '2018-03-23'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetUserShareHour(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetUpStreamMsg', () => {
        it('获取消息发送概况数据', async () => {
            const begin_date = '2018-03-20'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetUpStreamMsg(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetUpStreamMsgHour', () => {
        it('获取消息分送分时数据', async () => {
            const begin_date = '2018-03-23'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetUpStreamMsgHour(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetUpStreamMsgWeek', () => {
        it('获取消息发送周数据', async () => {
            const begin_date = '2018-02-23'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetUpStreamMsgWeek(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetUpStreamMsgMonth', () => {
        it('获取消息发送月数据', async () => {
            const begin_date = '2018-02-23'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetUpStreamMsgMonth(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetUpStreamMsgDist', () => {
        it('获取消息发送分布数据', async () => {
            const begin_date = '2018-03-13'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetUpStreamMsgDist(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetUpStreamMsgDistWeek', () => {
        it('获取消息发送分布周数据', async () => {
            const begin_date = '2018-03-13'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetUpStreamMsgDistWeek(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetUpStreamMsgDistMonth', () => {
        it('获取消息发送分布月数据', async () => {
            const begin_date = '2018-03-13'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetUpStreamMsgDistMonth(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetInterfaceSummary', () => {
        it('获取接口分析数据', async () => {
            const begin_date = '2018-03-13'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetInterfaceSummary(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    describe('#dataGetInterfaceSummaryHour', () => {
        it('获取接口分析分时数据', async () => {
            const begin_date = '2018-03-23'
            const end_date = '2018-03-23'
            const res = await wechat.dataGetInterfaceSummaryHour(begin_date, end_date)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('list')
        })
    })
    // TODO 卡券统计
    // describe('#dataCubeGetCardCardInfo', () => {
    //     it('获取接口分析分时数据', async () => {
    //         const begin_date = '2018-03-23'
    //         const end_date = '2018-03-23'
    //         const res = await wechat.dataCubeGetCardCardInfo(begin_date, end_date)
    //         console.log(res)
    //         expect(res).to.be.an('object')
    //         expect(res).to.have.property('list')
    //     })
    // })

    
})