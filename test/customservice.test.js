const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('customservice.js', () => {
    // describe('#kfAccountAdd', () => {
    //     it('添加客服帐号', async () => {
    //         const kf_account = "test1@test"
    //         const nickname = "客服1"
    //         const password = "pwdtest1"
    //         const res = await wechat.kfAccountAdd(kf_account, nickname, password)
    //         console.log(res)
    //         expect(res).to.be.an('object')
    //         expect(res).to.have.property('errcode')
    //     })
    // })
    // describe('#kfAccountUpdate', () => {
    //     it('更新客服帐号', async () => {
    //         const kf_account = "test1@test"
    //         const nickname = "客服1"
    //         const password = "pwdtestupdate"
    //         const res = await wechat.kfAccountUpdate(kf_account, nickname, password)
    //         console.log(res)
    //         expect(res).to.be.an('object')
    //         expect(res).to.have.property('errcode')
    //     })
    // })
    // describe('#kfAccountDel', () => {
    //     it('删除客服帐号', async () => {
    //         const kf_account = "test1@test"
    //         const nickname = "客服1"
    //         const password = "pwdtestupdate"
    //         const res = await wechat.kfAccountDel(kf_account, nickname, password)
    //         console.log(res)
    //         expect(res).to.be.an('object')
    //         expect(res).to.have.property('errcode')
    //     })
    // })
    // describe('#getKfList', () => {
    //     it('获取所有客服账号', async () => {
    //         const res = await wechat.getKfList()
    //         console.log(res)
    //         expect(res).to.be.an('object')
    //         expect(res).to.have.property('kf_list')
    //     })
    // })

    // describe('#customSend', () => {
    //     it('获取所有客服账号', async () => {
    //         const data = {
    //             touser: "ozP--s9_liC6beFdYa0AQySr0fEw",
    //             msgtype: "text",
    //             text: {
    //                 content: "Hello World"
    //             }
    //         }
    //         const res = await wechat.customSend(data)
    //         console.log(res)
    //         expect(res).to.be.an('object')
    //     })
    // })
    describe('#typing', () => {
        it('获取所有客服账号', async () => {
            const touser = 'OPENID'
            const command = 'Typing'
            const res = await wechat.typing(touser, command)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
})