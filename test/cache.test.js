const options = require('./config')
const Cache = require('siwi-cache')
const cache = new Cache(options.cache)
const expect = require('chai').expect

describe('cache.js 缓存模块测试', function () {
    describe('#put', function () {
        it('设置缓存', async function () {
            const res = await cache.set('mankong', 'hello mankong', 6000)
            expect(res).to.be.true
        })
    })
    describe('#get', function () {
        it('读取缓存', async function () {
            const res = await cache.get('mankong')
            expect(res).to.be.a('string')
        })
    })
})