const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('account.js', function () {
    describe('#qrcodeCreate', function () {
        it('创建临时二维码 sence_info 为sence_id', async function () {
            const sence_id = Math.floor(Math.random() * 10000)
            const res = await wechat.qrcodeCreate(86400 * 29, sence_id)
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('ticket')
        })
        it('创建临时二维码 sence_info 为sence_str', async function () {
            const sence_str = 'STR' + Math.floor(Math.random() * 100)
            const res = await wechat.qrcodeCreate(86400 * 29, sence_str)
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('ticket')
        })
        it('创建永久二维码 sence_info 为sence_id', async function () {
            const sence_id = Math.floor(Math.random() * 100000)
            const res = await wechat.qrcodeCreate(sence_id)
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('ticket')
        })
        it('创建永久二维码 sence_info 为sence_str', async function () {
            const sence_str = 'STR_LIMIT' + Math.floor(Math.random() * 100)
            const res = await wechat.qrcodeCreate(sence_str)
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('ticket')
        })
    })
    describe('#showQRCode', function () {
        it('通过ticket换取二维码 ', async function () {
            const sence_id = Math.floor(Math.random() * 10000)
            const qrcode = await wechat.qrcodeCreate(86400 * 29, sence_id)
            const ticket = qrcode['ticket']
            const res = await wechat.showQRCode(ticket)
            console.log(res)
            expect(res).to.be.a('String')
        })
    })
    describe('#longToShort', function () {
        it('通过ticket换取二维码 ', async function () {
            const long_url = `https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=gQEd8DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyeVc4aVpRY19hLVAxUHlFcXhxMUkAAgRi7bNaAwSAOyYA`
            const res = await wechat.longToShort(long_url)
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('short_url')
        })
    })
})