const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('comment.js', () => {
    describe('#commentOpen', () => {
        it('打开已群发文章评论', async () => {
            const msg_data_id = 1
            const index = 0
            const res = await wechat.commentOpen(msg_data_id, index)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#commentClose', () => {
        it('关闭已群发文章评论', async () => {
            const msg_data_id = 1
            const index = 0
            const res = await wechat.commentClose(msg_data_id, index)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#commentList', () => {
        it(' 查看指定文章的评论数据', async () => {
            const msg_data_id = 1
            const index = 0
            const begin = 0
            const count = 50
            const type = 0
            const res = await wechat.commentList(msg_data_id, index=0, begin, count, type)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#commentMarkelect', () => {
        it('将评论标记精选', async () => {
            const msg_data_id = 1
            const index = 0
            const user_comment_id = 100       
            const res = await wechat.commentMarkelect(msg_data_id, index=0, user_comment_id)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#commentUnMarkelect', () => {
        it('将评论取消精选', async () => {
            const msg_data_id = 1
            const index = 0
            const user_comment_id = 100       
            const res = await wechat.commentUnMarkelect(msg_data_id, index=0, user_comment_id)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#commentDelete', () => {
        it('删除评论', async () => {
            const msg_data_id = 1
            const index = 0
            const user_comment_id = 100       
            const res = await wechat.commentDelete(msg_data_id, index=0, user_comment_id)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#commentReplyAdd', () => {
        it('回复评论', async () => {
            const msg_data_id = 1
            const index = 0
            const user_comment_id = 100    
            const content = 'good'   
            const res = await wechat.commentReplyAdd(msg_data_id, index=0, user_comment_id, content)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#commentReplyDelete', () => {
        it('删除回复', async () => {
            const msg_data_id = 1
            const index = 0
            const user_comment_id = 100    
            const res = await wechat.commentReplyDelete(msg_data_id, index=0, user_comment_id)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
})