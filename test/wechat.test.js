const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('wechat.js', function () {
    // describe('#getAccessToken', function () {
    //     it('获取access_token', async function () {
    //         const res = await wechat.getAccessToken()
    //         expect(res).to.be.a('string')
    //     })
    // })
    // describe('#receiveMessages', function () {
    //     let xmlMsg
    //     beforeEach(function() {
    //         xmlMsg = `<xml>
    //         <ToUserName><![CDATA[toUser]]></ToUserName>
    //         <FromUserName><![CDATA[FromUser]]></FromUserName>
    //         <CreateTime>123456789</CreateTime>
    //         <MsgType><![CDATA[event]]></MsgType>
    //         <Event><![CDATA[CLICK]]></Event>
    //         <EventKey><![CDATA[EVENTKEY]]></EventKey>
    //         </xml>`
    //     })
    //     it('解析xml消息到json', async function () {
    //         const res = await wechat.receiveMessages(xmlMsg)
    //         expect(res).to.be.an('Object')
    //     })
    // })
    // describe('#generateNonceStr', () => {
    //     it('生成随机字符串', async () => {
    //         const res = await wechat.generateNonceStr()
    //         console.log(res)
    //         expect(res).to.be.a('string')
    //     });
    // });
    describe('#sign', () => {
        it('签名', async () => {
            const data = {
                appid: 'wxd930ea5d5a258f4f',
                mch_id: 10000100,
                device_info: 1000,
                body: 'test',
                test: '',
                nonce_str: 'ibuaiVcKdpRxkhJA',
            }
            const res = await wechat.sign(data)
            console.log(res)
            expect(res).to.be.a('string')
        });
    });
})