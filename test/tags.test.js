const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('user.js', function () {
    describe('#tagsCreate', function () {
        it('创建标签删除标签', async function () {
            const name = 'tag_create_test'
            const res = await wechat.tagsCreate(name)
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('tag')
            const id = res['tag']['id']
            await wechat.tagsDelete(id)
        })
    })
    describe('#tagsUpdate', function () {
        let id = ''
        beforeEach(async function () {
            tag = await wechat.tagsCreate('tags_update_test')
            console.log(tag)
            id = tag['tag']['id']
        })
        it('更新标签', async function () {
            const tag = {
                id: id,
                name: 'tags_update_test2'
            }
            const res = await wechat.tagsUpdate(tag)
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
            await wechat.tagsDelete(id)
        })
    })
    describe('#tagsGet', function () {
        it('获取标签列表', async function () {
            const res = await wechat.tagsGet()
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('tags')
        })
    })
    describe('#userTagGet', function () {
        it('获取标签下用户', async function () {
            const tag_id = 2
            const res = await wechat.userTagGet(tag_id)
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('count')
        })
    })
    describe('#tagsMembersBatchTagging', function () {
        it('批量为用户打标签', async function () {
            const openid_list = [
                'ozP--s5Ez2jzjjRroUcTyAVz9EUs',
                'ozP--s9_liC6beFdYa0AQySr0fEw',
                'ozP--s3_IZfUCZI_NuLXs0KTuR08'
            ]
            const tag_id = 2
            const res = await wechat.tagsMembersBatchTagging(openid_list, tag_id)
            console.log(res)
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })
    describe('#tagsMembersBatchUnTagging', function () {
        it('批量为用户取消标签', async function () {
            const openid_list = [
                'ozP--s5Ez2jzjjRroUcTyAVz9EUs',
                'ozP--s9_liC6beFdYa0AQySr0fEw',
            ]
            const tag_id = 2
            const res = await wechat.tagsMembersBatchUnTagging(openid_list, tag_id)
            console.log(res)
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })
    describe('#tagsGetIdList', function () {
        it('获取用户身上的标签列表', async function () {
            const openid_list = [
                'ozP--s5Ez2jzjjRroUcTyAVz9EUs',
                'ozP--s9_liC6beFdYa0AQySr0fEw',
                'ozP--s3_IZfUCZI_NuLXs0KTuR08'
            ]
            const tag_id = 100
            await wechat.tagsMembersBatchTagging(openid_list, tag_id)
            const res = await wechat.tagsGetIdList(openid_list[2])
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('tagid_list')
        })
    })
    describe('#tagsMembersBatchBlackList', function () {
        it('拉黑用户', async function () {
            const openid_list = [
                'ozP--s5Ez2jzjjRroUcTyAVz9EUs',
                'ozP--s9_liC6beFdYa0AQySr0fEw'
            ]
            const res = await wechat.tagsMembersBatchBlackList(openid_list)
            console.log(res)
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })
    
    describe('#tagsMembersGetBlackList', function () {
        it('黑名单', async function () {
            const res = await wechat.tagsMembersGetBlackList()
            console.log(res)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('total')
        })
    })
    describe('#tagsMembersBatchUnBlackList', function () {
        it('取消拉黑用户', async function () {
            const openid_list = [
                'ozP--s5Ez2jzjjRroUcTyAVz9EUs',
                'ozP--s9_liC6beFdYa0AQySr0fEw',
                'ozP--s3_IZfUCZI_NuLXs0KTuR08'
            ]
            const res = await wechat.tagsMembersBatchUnBlackList(openid_list)
            console.log(res)
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })
})