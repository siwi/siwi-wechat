const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('user.js', function () {
    describe('#userGet', function () {
        it('获取用户列表', async function () {
            const res = await wechat.userGet()
            expect(res).to.be.an('Object')
            expect(res).to.have.property('data')
        })
    })
    describe('#userInfo', function () {
        it('获取用户信息', async function () {
            const users = await wechat.userGet()
            openid = users['data']['openid'][0]
            const res = await wechat.userInfo(openid)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('openid')
        })
    })
    describe('#userInfoBatchGet', function () {
        it('批量获取用户信息', async function () {
            const user_list = [{
                "openid": "ozP--s5Ez2jzjjRroUcTyAVz9EUs",
                "lang": "zh_CN"
            },{
                "openid": "ozP--s9_liC6beFdYa0AQySr0fEw",
                "lang": "zh_CN"
            },{
                "openid": "ozP--s3_IZfUCZI_NuLXs0KTuR08",
                "lang": "zh_CN"
            }]
            const res = await wechat.userInfoBatchGet(user_list)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('user_info_list')
        })
    })
    describe('#userInfoUpdateRemark', function () {
        it('设置用户备注名', async function () {
            const openid = 'ozP--s3_IZfUCZI_NuLXs0KTuR08'
            const remark = 'siwi-logger'
            const res = await wechat.userInfoUpdateRemark(openid, remark)
            expect(res).to.be.an('Object')
            expect(res).to.have.property('errcode')
            expect(res.errcode).to.equal(0)
        })
    })
})