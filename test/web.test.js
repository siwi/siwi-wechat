const options = require('./config')
const Wechat = require('../index')
const wechat = new Wechat(options)
const expect = require('chai').expect

describe('web.js', () => {
    describe('#oauth2Authorize', () => {
        it('用户同意授权，获取code', async () => {
            const state = Math.floor(Math.random() * 10)
            const redirect_uri = 'https://sznzkj.cn'
            const scope = 'snsapi_userinfo'
            const res = await wechat.oauth2Authorize(redirect_uri, scope, state)
            console.log(res)
            expect(res).to.be.a('string')
        })
    })

    describe('#snsOauth2AccessToken', () => {
        it('通过code换取网页授权access_token', async () => {
            const code = '0818KFSO1n7DE31oAGTO1m2CSO18KFS5'
            const res = await wechat.snsOauth2AccessToken(code)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#snsUserInfo', () => {
        it('拉取用户信息(需scope为 snsapi_userinfo)', async () => {
            const access_token = '8_iTyO63LkdS0v-tQhazQYkVvkr2YRzMzNl1gkL2UHYCdBvXV0F-JbcdOuXhRlFex4D7RK1zUJE-cQuobJzZmYOg'
            const openid = 'ozP--s3_IZfUCZI_NuLXs0KTuR08'
            const res = await wechat.snsUserInfo(access_token, openid)
            console.log(res)
            expect(res).to.be.an('object')
        })
    })
    describe('#snsAuth', () => {
        it('检验授权凭证（access_token）是否有效', async () => {
            const access_token = '8_iTyO63LkdS0v-tQhazQYkVvkr2YRzMzNl1gkL2UHYCdBvXV0F-JbcdOuXhRlFex4D7RK1zUJE-cQuobJzZmYOg'
            const openid = 'ozP--s3_IZfUCZI_NuLXs0KTuR08'
            const res = await wechat.snsAuth(access_token, openid)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('errcode')
            expect(res).to.equal(0)
        })
    })
     describe('#snsOauth2RefreshToken', () => {
        it('刷新access_token（如果需要）', async () => {
            const refresh_token = '8_y_cUCeFrc0YFQzNgUH8FdOil62Ytr9nrIX7AzbbHlF4VFtZjCkuCmvrbZ2qiwpwaGT_58Xn3-JG3Q8sExMZgZg'
            const res = await wechat.snsOauth2RefreshToken(refresh_token)
            console.log(res)
            expect(res).to.be.an('object')
            expect(res).to.have.property('access_token')
        })
    })
})